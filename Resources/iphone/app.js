function processJson(json) {
    var length = json.posts.length;
    Ti.API.info("length: " + length);
    for (var i = 0; length > i; i++) {
        var wpID = json.posts[i].id;
        var url = json.posts[i].url;
        var title = json.posts[i].title;
        var date = json.posts[i].date;
        var modified = json.posts[i].modified;
        var serves = json.posts[i].custom_fields.serves[0];
        var preparation = json.posts[i].custom_fields.preparation[0];
        var ingredients = json.posts[i].custom_fields.ingredients[0];
        var ph_information = json.posts[i].custom_fields.ph_information[0];
        var ph_bracket = json.posts[i].custom_fields.ph_bracket[0];
        var mealtime = json.posts[i].custom_fields.mealtime[0];
        var imageID = "";
        var image = "";
        var thumb = "";
        var aImages = json.posts[i].attachments;
        if (aImages.length > 0) {
            imageID = aImages[0].id;
            image = aImages[0].images.medium.url;
            thumb = aImages[0].images.thumbnail.url;
        }
        var row = db.execute("SELECT wpID,modified FROM recipes WHERE wpID=" + wpID);
        if (row.isValidRow()) {
            if (modified > row.fieldByName("modified")) {
                Ti.API.info("updating version");
                db.execute('UPDATE recipes SET url="' + url + '", title="' + title + '", date="' + date + '", modified="' + modified + '", serves="' + serves + '", preparation="' + preparation + '", ingredients="' + ingredients + '", ph_information="' + ph_information + '", ph_bracket="' + ph_bracket + '", mealtime="' + mealtime + '", imageID="' + imageID + '",  image="' + image + '", thumb="' + thumb + '" WHERE wpID=' + wpID);
                cachedImageView(wpID, image);
            }
        } else {
            db.execute("INSERT INTO recipes (wpID,url,title,date,modified,serves,preparation,ingredients,ph_information,ph_bracket,mealtime, imageID, image, thumb) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)", wpID, url, title, date, modified, serves, preparation, ingredients, ph_information, ph_bracket, mealtime, imageID, image, thumb);
            var imageSaved = cachedImageView(wpID, image);
            imageSaved ? Ti.API.info("inserting:" + wpID + " - image saved") : Ti.API.info("inserting:" + wpID + " - NO image saved");
        }
    }
    db.close();
}

function cachedImageView(imageDirectoryName, url) {
    var filename = url.split("/");
    filename = filename[filename.length - 1];
    var file = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, imageDirectoryName, filename);
    var g = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, imageDirectoryName);
    g.exists() || g.createDirectory();
    var xhr = Ti.Network.createHTTPClient();
    xhr.onload = function() {
        if (200 == xhr.status) {
            file.write(xhr.responseData);
            Ti.API.info("image: " + filename + " - saved locally on folder: " + imageDirectoryName);
            return true;
        }
    };
    xhr.open("GET", url);
    xhr.send();
}

var Alloy = require("alloy"), _ = Alloy._, Backbone = Alloy.Backbone;

Alloy.Globals.trackImages_5_6 = "images/slider/slider-track-5,6.png";

Alloy.Globals.trackImages_5_9 = "images/slider/slider-track-5,9.png";

Alloy.Globals.trackImages_6_2 = "images/slider/slider-track-6,2.png";

Alloy.Globals.trackImages_6_5 = "images/slider/slider-track-6,5.png";

Alloy.Globals.trackImages_6_8 = "images/slider/slider-track-6,8.png";

Alloy.Globals.trackImages_7_0 = "images/slider/slider-track-7,0.png";

Alloy.Globals.trackImages_7_2 = "images/slider/slider-track-7,2.png";

Alloy.Globals.trackImages_7_4 = "images/slider/slider-track-7,4.png";

Alloy.Globals.trackImages_7_7 = "images/slider/slider-track-7,7.png";

Alloy.Globals.trackImages_8_0 = "images/slider/slider-track-8,0.png";

Alloy.Globals.thumbImages_5_6 = "images/slider/thumb_5-6.png";

Alloy.Globals.thumbImages_5_9 = "images/slider/thumb_5-9.png";

Alloy.Globals.thumbImages_6_2 = "images/slider/thumb_6-2.png";

Alloy.Globals.thumbImages_6_5 = "images/slider/thumb_6-5.png";

Alloy.Globals.thumbImages_6_8 = "images/slider/thumb_6-8.png";

Alloy.Globals.thumbImages_7_0 = "images/slider/thumb_7-0.png";

Alloy.Globals.thumbImages_7_2 = "images/slider/thumb_7-2.png";

Alloy.Globals.thumbImages_7_4 = "images/slider/thumb_7-4.png";

Alloy.Globals.thumbImages_7_7 = "images/slider/thumb_7-7.png";

Alloy.Globals.thumbImages_8_0 = "images/slider/thumb_8-0.png";

Alloy.Globals.trackBg_5_6 = "images/slider/entry-5-6.png";

Alloy.Globals.trackBg_5_9 = "images/slider/entry-5-9.png";

Alloy.Globals.trackBg_6_2 = "images/slider/entry-6-2.png";

Alloy.Globals.trackBg_6_5 = "images/slider/entry-6-5.png";

Alloy.Globals.trackBg_6_8 = "images/slider/entry-6-8.png";

Alloy.Globals.trackBg_7_0 = "images/slider/entry-7-0.png";

Alloy.Globals.trackBg_7_2 = "images/slider/entry-7-2.png";

Alloy.Globals.trackBg_7_4 = "images/slider/entry-7-4.png";

Alloy.Globals.trackBg_7_7 = "images/slider/entry-7-7.png";

Alloy.Globals.trackBg_8_0 = "images/slider/entry-8-0.png";

Alloy.Globals.questionMark_5_6 = "images/slider/questionMark-5-6.png";

Alloy.Globals.questionMark_5_9 = "images/slider/questionMark-5-9.png";

Alloy.Globals.questionMark_6_2 = "images/slider/questionMark-6-2.png";

Alloy.Globals.questionMark_6_5 = "images/slider/questionMark-6-5.png";

Alloy.Globals.questionMark_6_8 = "images/slider/questionMark-6-8.png";

Alloy.Globals.questionMark_7_0 = "images/slider/questionMark-7-0.png";

Alloy.Globals.questionMark_7_2 = "images/slider/questionMark-7-2.png";

Alloy.Globals.questionMark_7_4 = "images/slider/questionMark-7-4.png";

Alloy.Globals.questionMark_7_7 = "images/slider/questionMark-7-7.png";

Alloy.Globals.questionMark_8_0 = "images/slider/questionMark-8-0.png";

Alloy.Globals.color_5_0 = "#E0703B";

Alloy.Globals.color_5_2 = "#E1753C";

Alloy.Globals.color_5_4 = "#EA9344";

Alloy.Globals.color_5_6 = "#F0A84D";

Alloy.Globals.color_5_8 = "#F1B250";

Alloy.Globals.color_6_0 = "#E5B74B";

Alloy.Globals.color_6_2 = "#D3BD47";

Alloy.Globals.color_6_4 = "#CCBE45";

Alloy.Globals.color_6_6 = "#9fc63e";

Alloy.Globals.color_6_8 = "#72A963";

Alloy.Globals.color_7_0 = "#7FBB9B";

Alloy.Globals.color_7_2 = "#5BA684";

Alloy.Globals.color_7_4 = "#49979A";

Alloy.Globals.color_7_6 = "#57B1B6";

Alloy.Globals.color_7_8 = "#52ADBC";

Alloy.Globals.color_8_0 = "#4DAABF";

Alloy.Globals.color_8_2 = "#4CA9C0";

Alloy.Globals.color_8_4 = "#449DBF";

Alloy.Globals.color_8_6 = "#3E90BD";

Alloy.Globals.color_8_8 = "#3785BC";

Alloy.Globals.color_9_0 = "#197dbe";

var db = Ti.Database.open("recipes");

db.execute("CREATE TABLE IF NOT EXISTS recipes (id INTEGER PRIMARY KEY, wpID INTEGER, url TEXT, title TEXT, date TEXT, modified TEXT, serves TEXT, preparation TEXT, ingredients TEXT, ph_information TEXT, ph_bracket TEXT, mealtime TEXT,imageID INTEGER, image TEXT, thumb TEXT);");

db.file.setRemoteBackup(false);

var client = Ti.Network.createHTTPClient();

client.onload = function() {
    var json = JSON.parse(client.responseText);
    Ti.API.info("procesing online json");
    processJson(json);
};

client.onerror = function() {
    var file = Ti.Filesystem.getFile("data/recipes.json");
    var data = file.read().text;
    var json = JSON.parse(data);
    Ti.API.info("procesing local json");
    processJson(json);
};

client.setTimeout(5e3);

client.open("POST", "http://goph.recipes/api/get_posts/?post_type=recipe&count=100");

client.send();

Alloy.createController("index");