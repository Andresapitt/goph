function AspectRatio(width, height, maxWidth, maxHeight) {
    var ratio = 0;
    if (width > maxWidth) {
        ratio = maxWidth / width;
        height *= ratio;
        width *= ratio;
    }
    if (height > maxHeight) {
        ratio = maxHeight / height;
        width *= ratio;
        height *= ratio;
    }
    this.width = width;
    this.height = height;
}

function PagingControl(scrollableView) {
    var pages = [];
    var page;
    var numberOfPages = 0;
    var pageColor = "#c99ed5";
    var container = Titanium.UI.createView({
        height: 60
    });
    numberOfPages = scrollableView.getViews().length;
    pages = [];
    for (var i = 0; numberOfPages > i; i++) {
        page = Titanium.UI.createView({
            borderRadius: 4,
            width: 8,
            height: 8,
            left: 15 * i,
            backgroundColor: pageColor,
            opacity: .5
        });
        pages.push(page);
        container.add(page);
    }
    pages[scrollableView.getCurrentPage()].setOpacity(1);
    scrollableView.addEventListener("scroll", onScroll);
    scrollableView.addEventListener("postlayout", onPostLayout);
    return container;
}

module.exports = AspectRatio;

onScroll = function(event) {
    for (var i = 0; numberOfPages > i; i++) pages[i].setOpacity(.5);
    pages[event.currentPage].setOpacity(1);
};

onPostLayout = function() {
    for (var i = 0; numberOfPages > i; i++) pages[i].setOpacity(.5);
    pages[scrollableView.currentPage].setOpacity(1);
};

module.exports = PagingControl;