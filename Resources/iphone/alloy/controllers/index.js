function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    function updateLastReading() {
        $.lastReadingLabel.text = Ti.App.Properties.getString("lastReading", "7.0");
        $.lastReadingLabel.color = Ti.App.Properties.getString("currentColour", "#5abf93");
        $.lastReadingLabelb.text = Ti.App.Properties.getString("lastReading", "7.0");
        $.lastReadingLabelb.color = Ti.App.Properties.getString("currentColour", "#5abf93");
        var now = new Date();
        var now2 = new Date();
        now.setDate(now.getDate() - 1);
        now2.setDate(now.getDate() - 2);
        var month = "";
        switch (now.getMonth()) {
          case 0:
            month = "January";
            break;

          case 1:
            month = "February";
            break;

          case 2:
            month = "March";
            break;

          case 3:
            month = "April";
            break;

          case 4:
            month = "May";
            break;

          case 5:
            month = "June";
            break;

          case 6:
            month = "July";
            break;

          case 7:
            month = "August";
            break;

          case 8:
            month = "September";
            break;

          case 9:
            month = "October";
            break;

          case 10:
            month = "November";
            break;

          case 11:
            month = "December";
        }
        $.lastReadingDate.text = now.getDate() + ", " + month + " " + now.getFullYear() + " | Week 1 | Day 2";
        $.lastReadingDateb.text = now2.getDate() + ", " + month + " " + now2.getFullYear() + " | Week 1 | Day 1";
    }
    function notImplemented() {
        alert("featured not yet implemented");
    }
    function changeColor(value) {
        var bgcolor = "";
        value >= 5 && 5.2 > value ? bgcolor = Alloy.Globals.color_5_0 : value >= 5.2 && 5.4 > value ? bgcolor = Alloy.Globals.color_5_2 : value >= 5.4 && 5.6 > value ? bgcolor = Alloy.Globals.color_5_4 : value >= 5.6 && 5.8 > value ? bgcolor = Alloy.Globals.color_5_6 : value >= 5.8 && 6 > value ? bgcolor = Alloy.Globals.color_5_8 : value >= 6 && 6.2 > value ? bgcolor = Alloy.Globals.color_6_0 : value >= 6.2 && 6.4 > value ? bgcolor = Alloy.Globals.color_6_2 : value >= 6.4 && 6.6 > value ? bgcolor = Alloy.Globals.color_6_4 : value >= 6.6 && 6.8 > value ? bgcolor = Alloy.Globals.color_6_6 : value >= 6.8 && 7 > value ? bgcolor = Alloy.Globals.color_6_8 : value >= 7 && 7.2 > value ? bgcolor = Alloy.Globals.color_7_0 : value >= 7.2 && 7.4 > value ? bgcolor = Alloy.Globals.color_7_2 : value >= 7.4 && 7.6 > value ? bgcolor = Alloy.Globals.color_7_4 : value >= 7.6 && 7.8 > value ? bgcolor = Alloy.Globals.color_7_6 : value >= 7.8 && 8 > value ? bgcolor = Alloy.Globals.color_7_8 : value >= 8 && 8.2 > value ? bgcolor = Alloy.Globals.color_8_0 : value >= 8.2 && 8.4 > value ? bgcolor = Alloy.Globals.color_8_2 : value >= 8.4 && 8.6 > value ? bgcolor = Alloy.Globals.color_8_4 : value >= 8.6 && 8.8 > value ? bgcolor = Alloy.Globals.color_8_6 : value >= 8.8 && 9 > value ? bgcolor = Alloy.Globals.color_8_8 : value >= 9 && (bgcolor = Alloy.Globals.color_9_0);
        Ti.App.fireEvent("app:changeColor", {
            mycolor: bgcolor
        });
        if (6.9 >= value) {
            $.neutralView.backgroundImage = "images/ph/neutral.png";
            $.alkalineView.backgroundImage = "images/ph/alkaline.png";
            $.acidicView.backgroundImage = "images/ph/acidic_over.png";
        } else if (value >= 7.1) {
            $.neutralView.backgroundImage = "images/ph/neutral.png";
            $.alkalineView.backgroundImage = "images/ph/alkaline_over.png";
            $.acidicView.backgroundImage = "images/ph/acidic.png";
        } else {
            $.neutralView.backgroundImage = "images/ph/neutral_over.png";
            $.alkalineView.backgroundImage = "images/ph/alkaline.png";
            $.acidicView.backgroundImage = "images/ph/acidic.png";
        }
        $.readingLabel.color = bgcolor;
        $.saveBtn.backgroundColor = bgcolor;
    }
    function saveReading() {
        if ("remedy" != $.saveBtn.title) {
            Ti.App.Properties.setString("lastReading", numb);
            updateLastReading();
            $.saveBtn.title = "remedy";
            Ti.App.Properties.setString("currentColour", $.saveBtn.backgroundColor);
        } else {
            var winTimesOfDay = Alloy.createController("times-of-day").getView();
            Alloy.Globals.navGroup.openWindow(winTimesOfDay);
            $.saveBtn.title = "save";
        }
    }
    function openCaveat() {
        var caveatWin = Alloy.createController("caveat").getView();
        Alloy.Globals.navGroup.openWindow(caveatWin);
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "index";
    if (arguments[0]) {
        __processArg(arguments[0], "__parentSymbol");
        __processArg(arguments[0], "$model");
        __processArg(arguments[0], "__itemTemplate");
    }
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.win1 = Ti.UI.createWindow({
        id: "win1",
        title: "Track",
        backgroundColor: "#FFFFFF",
        barColor: "#5abf93"
    });
    $.__views.__alloyId0 = Ti.UI.createView({
        backgroundColor: "#ffffff",
        width: Ti.UI.FULL,
        height: Ti.UI.SIZE,
        top: "0",
        id: "__alloyId0"
    });
    $.__views.win1.add($.__views.__alloyId0);
    $.__views.enterReadingLabel = Ti.UI.createLabel({
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        color: "#c0c0c1",
        font: {
            fontFamily: "Roboto-Regular",
            fontSize: "11dp",
            fontStyle: "normal",
            fontWeight: "normal"
        },
        text: "Please enter your reading",
        id: "enterReadingLabel",
        top: "10",
        zIndex: "100"
    });
    $.__views.__alloyId0.add($.__views.enterReadingLabel);
    $.__views.webview = Ti.UI.createWebView({
        id: "webview",
        url: "slider/index.html",
        width: "280",
        height: "221",
        top: "32",
        showScrollbars: "false",
        disableBounce: "true"
    });
    $.__views.__alloyId0.add($.__views.webview);
    $.__views.sliderBg = Ti.UI.createView({
        touchEnabled: false,
        id: "sliderBg",
        width: "320",
        height: "225",
        top: "0",
        backgroundImage: "images/slider/entry_slider_bg.png"
    });
    $.__views.__alloyId0.add($.__views.sliderBg);
    $.__views.yourPh = Ti.UI.createLabel({
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        color: "#d8d8d9",
        font: {
            fontFamily: "Roboto-Light",
            fontSize: "11dp"
        },
        text: "your pH",
        id: "yourPh",
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        top: "90"
    });
    $.__views.__alloyId0.add($.__views.yourPh);
    $.__views.readingLabel = Ti.UI.createLabel({
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        color: "#4bc1d2",
        font: {
            fontFamily: "Roboto-Light",
            fontSize: "50dp"
        },
        text: "7.0",
        id: "readingLabel",
        top: "100",
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER
    });
    $.__views.__alloyId0.add($.__views.readingLabel);
    $.__views.saveBtn = Ti.UI.createButton({
        backgroundColor: "#4bc1d2",
        color: "#fff",
        font: {
            fontFamily: "Roboto-Bold"
        },
        id: "saveBtn",
        title: "save",
        top: "216",
        height: "23",
        width: "74"
    });
    $.__views.__alloyId0.add($.__views.saveBtn);
    saveReading ? $.__views.saveBtn.addEventListener("click", saveReading) : __defers["$.__views.saveBtn!click!saveReading"] = true;
    $.__views.caveatView = Ti.UI.createView({
        id: "caveatView",
        top: "260",
        height: "31",
        width: Ti.UI.FILL,
        backgroundColor: "#5abf93"
    });
    $.__views.win1.add($.__views.caveatView);
    $.__views.caveatLabel = Ti.UI.createLabel({
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        color: "#ffffff",
        font: {
            fontFamily: "Roboto-Regular",
            fontSize: "12dp",
            fontStyle: "normal",
            fontWeight: "normal"
        },
        text: "if your reading is below 5 contact your GP",
        id: "caveatLabel",
        left: "20"
    });
    $.__views.caveatView.add($.__views.caveatLabel);
    $.__views.caveatBtn = Ti.UI.createButton({
        id: "caveatBtn",
        title: "",
        right: "25",
        width: "14",
        height: "14",
        backgroundImage: "/images/arrow.png"
    });
    $.__views.caveatView.add($.__views.caveatBtn);
    $.__views.lastReadingContainer = Ti.UI.createView({
        id: "lastReadingContainer",
        visible: "false",
        height: "200",
        top: "295"
    });
    $.__views.win1.add($.__views.lastReadingContainer);
    notImplemented ? $.__views.lastReadingContainer.addEventListener("click", notImplemented) : __defers["$.__views.lastReadingContainer!click!notImplemented"] = true;
    $.__views.lastReadingTitle = Ti.UI.createLabel({
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        color: "#5abf93",
        font: {
            fontFamily: "Roboto-Regular",
            fontSize: "11dp",
            fontStyle: "normal",
            fontWeight: "normal"
        },
        text: "YOUR LAST READINGS:",
        id: "lastReadingTitle",
        top: "0",
        left: "20",
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT
    });
    $.__views.lastReadingContainer.add($.__views.lastReadingTitle);
    $.__views.lastReading1 = Ti.UI.createView({
        id: "lastReading1",
        top: "20",
        width: Ti.UI.FILL,
        height: "27",
        backgroundColor: "#f8f8f8"
    });
    $.__views.lastReadingContainer.add($.__views.lastReading1);
    $.__views.__alloyId1 = Ti.UI.createView({
        touchEnabled: false,
        height: "1",
        width: Ti.UI.FILL,
        top: "0",
        backgroundColor: "#f4f4f4",
        id: "__alloyId1"
    });
    $.__views.lastReading1.add($.__views.__alloyId1);
    $.__views.lastReadingDate = Ti.UI.createLabel({
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        color: "#a6a6a8",
        font: {
            fontFamily: "Roboto-Regular",
            fontSize: "11dp",
            fontStyle: "normal",
            fontWeight: "normal"
        },
        touchEnabled: false,
        id: "lastReadingDate",
        left: "40",
        textAlign: Titanium.UI.TEXT_ALIGNMENT_RIGHT
    });
    $.__views.lastReading1.add($.__views.lastReadingDate);
    $.__views.lastReadingLabel = Ti.UI.createLabel({
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        color: "#4bc1d2",
        font: {
            fontFamily: "Roboto-Light",
            fontSize: "12dp"
        },
        touchEnabled: false,
        id: "lastReadingLabel",
        right: "20",
        textAlign: Titanium.UI.TEXT_ALIGNMENT_RIGHT
    });
    $.__views.lastReading1.add($.__views.lastReadingLabel);
    $.__views.__alloyId2 = Ti.UI.createView({
        touchEnabled: false,
        height: "1",
        width: Ti.UI.FILL,
        bottom: "0",
        backgroundColor: "#ededed",
        id: "__alloyId2"
    });
    $.__views.lastReading1.add($.__views.__alloyId2);
    $.__views.lastReading2 = Ti.UI.createView({
        id: "lastReading2",
        top: "47",
        width: Ti.UI.FILL,
        height: "27",
        backgroundColor: "#f8f8f8"
    });
    $.__views.lastReadingContainer.add($.__views.lastReading2);
    $.__views.__alloyId3 = Ti.UI.createView({
        touchEnabled: false,
        height: "1",
        width: Ti.UI.FILL,
        top: "0",
        backgroundColor: "#f4f4f4",
        id: "__alloyId3"
    });
    $.__views.lastReading2.add($.__views.__alloyId3);
    $.__views.lastReadingDateb = Ti.UI.createLabel({
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        color: "#a6a6a8",
        font: {
            fontFamily: "Roboto-Regular",
            fontSize: "11dp",
            fontStyle: "normal",
            fontWeight: "normal"
        },
        touchEnabled: false,
        id: "lastReadingDateb",
        left: "40",
        textAlign: Titanium.UI.TEXT_ALIGNMENT_RIGHT
    });
    $.__views.lastReading2.add($.__views.lastReadingDateb);
    $.__views.lastReadingLabelb = Ti.UI.createLabel({
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        color: "#4bc1d2",
        font: {
            fontFamily: "Roboto-Light",
            fontSize: "12dp"
        },
        touchEnabled: false,
        id: "lastReadingLabelb",
        right: "20",
        textAlign: Titanium.UI.TEXT_ALIGNMENT_RIGHT
    });
    $.__views.lastReading2.add($.__views.lastReadingLabelb);
    $.__views.__alloyId4 = Ti.UI.createView({
        touchEnabled: false,
        height: "1",
        width: Ti.UI.FILL,
        bottom: "0",
        backgroundColor: "#ededed",
        id: "__alloyId4"
    });
    $.__views.lastReading2.add($.__views.__alloyId4);
    $.__views.phInfo = Ti.UI.createView({
        id: "phInfo",
        backgroundImage: "images/phInfo_bg.png",
        "with": "320",
        height: "116",
        bottom: "0"
    });
    $.__views.win1.add($.__views.phInfo);
    $.__views.phInfoTitle = Ti.UI.createLabel({
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        color: "#959595",
        font: {
            fontFamily: "Roboto-Bold",
            fontSize: "13dp"
        },
        text: "Information about pH",
        id: "phInfoTitle",
        top: "17",
        left: "35"
    });
    $.__views.phInfo.add($.__views.phInfoTitle);
    $.__views.__alloyId5 = Ti.UI.createScrollView({
        left: "35",
        height: "85",
        top: "33",
        id: "__alloyId5"
    });
    $.__views.phInfo.add($.__views.__alloyId5);
    $.__views.phInfoLabel = Ti.UI.createLabel({
        width: "190",
        height: Ti.UI.FULL,
        color: "#a1a1a1",
        font: {
            fontFamily: "Roboto-Regular",
            fontSize: "12dp",
            fontStyle: "light"
        },
        text: "The human body requires many substances to function. We know that amino acids are essential and come in many forms. Sugars, fatty acids, many minerals and trace elements are essential ingredients. In spite of such a wide variety of components they can be divided roughly into two groups. Those which are alkaline (basic) substances and those which are acid. Both groups complement each other as nature intended. It is the balance between alkalinity and acidity which this GOph app with its healthy recipes intends to achieve by a diet which is acid-alkaline balanced. Following suggested diets which are so balanced to achieve alkalines and acids in equal amounts is the first step in healthy living.\n\nThe degree of acidity of a substance is measured by determining its pH. Before you entered the pH value reading into this app, you got the reading by placing the litmus paper under your tongue. The subsequent change in colour, the reaction, when the litmus paper came into contact with your saliva is an indication of the acidity or alkalinity of your body system. Strong acids in the body system, where the reading results will be in the 5 to 6 range come through ingesting meats consisting of uric, sulphuric and phosphoric acids. Excess of of acid elements are stored in the tissues. The recipes in the GOpH app are so designed that where animal protein is an optional diet such diets should only be taken where the body is already in an alkaline state, that is where a pH reading is above 7.\n\nUnlike acidic food elements alkaline substances are gentle on the body system, like bananas, milk and almonds where there are no traces of acidifying elements. Consequently, many of the diets in this app embrace recipes rich in alkaline forming foods. Sodium, magnesium and calcium minerals feature considerably in GOpH recipes and are essential if the body system is to be prevented from extracting these minerals from the bones and tissues in an attempt to maintain the all important  pH balance of 7.\n\nThe pH measuring scale is from 0 to 14. The neutral 7 is the ideal balance and represents perfect acid alkaline balance. Technically, the difference between alkalines and acids is determined by their ability to free hydrogen ions. As a result the more acid the food the greater potential to free hydrogen ions and thus the acidity range is low and expressed as a low pH value. In foods these will generally be from pH 5 to pH 6.8. Increments after pH 7 are regarded as alkaline. As a rule of thumb the lower the pH reading the greater the acidy in the food and the higher the pH reading. Further information can be had by going to the website www.goph.recipes",
        id: "phInfoLabel",
        top: "0",
        left: "0",
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT
    });
    $.__views.__alloyId5.add($.__views.phInfoLabel);
    $.__views.acidicView = Ti.UI.createButton({
        id: "acidicView",
        backgroundImage: "images/ph/acidic.png",
        width: "64",
        height: "27",
        top: "27",
        left: "243"
    });
    $.__views.phInfo.add($.__views.acidicView);
    $.__views.neutralView = Ti.UI.createButton({
        id: "neutralView",
        backgroundImage: "images/ph/neutral.png",
        width: "64",
        height: "27",
        top: "55",
        left: "243"
    });
    $.__views.phInfo.add($.__views.neutralView);
    $.__views.alkalineView = Ti.UI.createButton({
        id: "alkalineView",
        backgroundImage: "images/ph/alkaline.png",
        width: "64",
        height: "27",
        top: "84",
        left: "243"
    });
    $.__views.phInfo.add($.__views.alkalineView);
    $.__views.winMain = Ti.UI.iOS.createNavigationWindow({
        window: $.__views.win1,
        id: "winMain",
        backgroundColor: "#5abf93",
        backgroundTopCap: "#5abf93",
        tintColor: "#5abf93",
        backgroundImage: "images/navigationWindowBG.png"
    });
    $.__views.winMain && $.addTopLevelView($.__views.winMain);
    exports.destroy = function() {};
    _.extend($, $.__views);
    Alloy.Globals.navGroup = $.winMain;
    $.winMain.opacity = 1;
    $.win1.titleAttributes = {
        color: "#FFF",
        font: {
            fontFamily: "Roboto-Bold",
            fontSize: 16
        }
    };
    var numb;
    var iPhone5 = 568 === Ti.Platform.displayCaps.platformHeight;
    if (iPhone5) {
        $.lastReadingContainer.visible = true;
        updateLastReading();
    }
    $.caveatView.addEventListener("click", openCaveat);
    $.acidicView.addEventListener("click", function() {
        var acidicInfo = Alloy.createController("acidic-info").getView();
        Alloy.Globals.navGroup.openWindow(acidicInfo);
    });
    $.neutralView.addEventListener("click", function() {
        var neutralInfo = Alloy.createController("neutral-info").getView();
        Alloy.Globals.navGroup.openWindow(neutralInfo);
    });
    $.alkalineView.addEventListener("click", function() {
        var alkalineInfo = Alloy.createController("alkaline-info").getView();
        Alloy.Globals.navGroup.openWindow(alkalineInfo);
    });
    Ti.App.addEventListener("app:fromWebView", function(e) {
        Ti.API.info("value:" + e.value);
        numb = e.value;
        numb = numb.toFixed(1);
        $.readingLabel.text = numb;
    });
    Ti.App.addEventListener("app:release", function(e) {
        numb = e.value;
        numb = numb.toFixed(1);
        $.readingLabel.text = numb;
        changeColor(numb);
    });
    $.webview.addEventListener("load", function() {
        Ti.App.fireEvent("app:lastValue", {
            lastValue: Ti.App.Properties.getString("lastReading", "7.0")
        });
        $.readingLabel.text = Ti.App.Properties.getString("lastReading", "7.0");
        changeColor(Ti.App.Properties.getString("lastReading", "7.0"));
        Ti.API.info("last value: " + Ti.App.Properties.getString("lastReading", "7.0"));
    });
    var arrow1 = Titanium.UI.createMaskedImage({
        mask: "/images/arrow.png",
        tint: Ti.App.Properties.getString("currentColour", "#5abf93"),
        left: 15,
        width: 14,
        height: 14
    });
    $.lastReading1.add(arrow1);
    var arrow2 = Titanium.UI.createMaskedImage({
        mask: "/images/arrow.png",
        tint: Ti.App.Properties.getString("currentColour", "#5abf93"),
        left: 15,
        width: 14,
        height: 14
    });
    $.lastReading2.add(arrow2);
    $.winMain.open();
    __defers["$.__views.saveBtn!click!saveReading"] && $.__views.saveBtn.addEventListener("click", saveReading);
    __defers["$.__views.lastReadingContainer!click!notImplemented"] && $.__views.lastReadingContainer.addEventListener("click", notImplemented);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;