function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "caveat";
    if (arguments[0]) {
        __processArg(arguments[0], "__parentSymbol");
        __processArg(arguments[0], "$model");
        __processArg(arguments[0], "__itemTemplate");
    }
    var $ = this;
    var exports = {};
    $.__views.caveatWindow = Ti.UI.createWindow({
        backgroundImage: "images/main_bg.png",
        id: "caveatWindow",
        backgroundColor: "#ffffff",
        barColor: "#333333",
        title: "Caveat",
        titleControl: "#ffffff"
    });
    $.__views.caveatWindow && $.addTopLevelView($.__views.caveatWindow);
    $.__views.scrollView = Ti.UI.createScrollView({
        id: "scrollView",
        showVerticalScrollIndicator: "true",
        top: "5",
        height: Ti.UI.FILL,
        width: "90%"
    });
    $.__views.caveatWindow.add($.__views.scrollView);
    $.__views.mainText = Ti.UI.createLabel({
        color: "#9f9f9f",
        font: {
            fontFamily: "Helvetica",
            fontSize: "12dp",
            fontStyle: "normal",
            fontWeight: "normal"
        },
        text: "This GOpH app allows you the user to enter your pH readings to enable you to assess your body status, that is whether you are in an acidic state or alkaline state. A selection of recipes are available from which to choose according to your pH levels. These recipes have been carefully chosen for their alkalising properties which when consumed assists the body in changing from an unhealthy acidic state to a neutral and more alkaline healthier state. \n\nThe medical literature share the view that the ‘adoption of an alkaline diet can interrupt the acidification process and reduce the concentration of acids in the body, but diet alone is not sufficient to completely remove excess acids from the body’s internal environment” (1).\n\nWhere recipe suggestions have been made of a diet which addresses alkaline or acid imbalance it must be understood that these are only guidelines.\n\nHuman metabolism is not an exact science as stated by medical experts. Each person’s metabolism is different and should be understood in the context not only of dietary measures but lifestyle.\n\nThe GOpH app and its associated website http://www.goph.recipes contain statements which have not been medically verified and should be viewed in the context of a common understanding that vegetables and fruits have alkalising properties and are the food of choice for healthy living. Highly acidic foods such as meat and fish are acidifying.  The adoption of a diet where alkaline forming foods make up 80 percent of your plate and the remaining 20 percent includes acidifying foods  usually protein is commonly understood as a healthy approach to diet.\n\nIf you are re-evaluating a lifestyle programme which differs to what you have been used to you please consult your health care professional. The effects of diet are complex. The recipes on this app are devised for adults and modifying them for children or for persons with disabling conditions like diabetes or other metabolic conditions is the responsibility of the consumer. All information on the GOpH app and the GOpH.recipes  website is available for use by you the consumer and the publishers take no responsibility for any unlikely adverse reactions  consequential to our published recipes. \n\nYou are strongly advised to consult a qualified health care professional if you decide to vary any treatment plan you are currently following. We trust you enjoy using this app as a guide to maintaining a healthy dietary lifestyle. Understanding food and how some foods contribute to acidification in the human body, and taking remedial action is a prerequisite for healthy living. Understanding alkaline diets as a major contributory factor in rectifying acidification is common knowledge. Finally, the information contained in this app and its recommendations are based on consensus opinion and sound scientific research. Further information on healthy eating is on the website www.goph.recipes.\n\nReference (1) Vasey, C. (2006) The acid alkaline diet. Healing Hearts Press: Vermont.",
        id: "mainText",
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        top: "0",
        width: "90%"
    });
    $.__views.scrollView.add($.__views.mainText);
    exports.destroy = function() {};
    _.extend($, $.__views);
    $.caveatWindow.titleAttributes = {
        color: "white",
        font: {
            fontFamily: "Roboto-Bold",
            fontSize: 14
        }
    };
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;