function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "neutral-info";
    if (arguments[0]) {
        __processArg(arguments[0], "__parentSymbol");
        __processArg(arguments[0], "$model");
        __processArg(arguments[0], "__itemTemplate");
    }
    var $ = this;
    var exports = {};
    $.__views.neutralInfo = Ti.UI.createWindow({
        id: "neutralInfo",
        backgroundColor: "#ffffff",
        barColor: "#333333",
        backgroundImage: "images/main_bg.png",
        title: "Neutral",
        titleControl: "#ffffff",
        layout: "vertical"
    });
    $.__views.neutralInfo && $.addTopLevelView($.__views.neutralInfo);
    $.__views.scrollView = Ti.UI.createScrollView({
        id: "scrollView",
        showVerticalScrollIndicator: "true",
        top: "10",
        height: Ti.UI.FILL,
        width: "90%"
    });
    $.__views.neutralInfo.add($.__views.scrollView);
    $.__views.mainText = Ti.UI.createLabel({
        text: "Your results indicate that your pH range is in the neutral category. A pH reading of between pH 7 and pH 7.4 is a reading from a person in good health. If you are consistently in this range then your diet is satisfactorily maintaining an alkaline acid balance. To maintain this level it is strongly recommended to keep a menu diary listing food items you regularly eat. In this way if you do test for either acid or alkaline you have a record of what changes in your diet may have been responsible. It is wise to be aware of your own body metabolism which can change according to lifestyle variation where stress may have a negative effect. The recipes in this range have been created to maintain your pH levels within the 7 to 7.4 range. Further information is available on the GOpH.recipes website.",
        id: "mainText",
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        top: "0",
        width: "90%"
    });
    $.__views.scrollView.add($.__views.mainText);
    exports.destroy = function() {};
    _.extend($, $.__views);
    $.neutralInfo.titleAttributes = {
        color: "white",
        font: {
            fontFamily: "Roboto",
            fontSize: 14
        }
    };
    $.neutralInfo.navTintColor = "#ffffff";
    $.neutralInfo.barColor = Ti.App.Properties.getString("currentColour", "bgcolor");
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;