function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    function openDetails(evt) {
        var args = {
            data: $.recipeListing.title,
            recipeID: evt.source.recipeID
        };
        var winRecipeDetails = Alloy.createController("recipe-details", args).getView();
        Alloy.Globals.navGroup.openWindow(winRecipeDetails);
    }
    function resizeKeepAspectRatioNewWidth(blob, imageWidth, imageHeight, newWidth) {
        if (0 >= imageWidth || 0 >= imageHeight || 0 >= newWidth) return blob;
        var ratio = imageWidth / imageHeight;
        var w = newWidth;
        var h = newWidth / ratio;
        Ti.API.info("ratio: " + ratio);
        Ti.API.info("w: " + w);
        Ti.API.info("h: " + h);
        var resized = ImageFactory.imageAsResized(blob, {
            width: w,
            height: h
        });
        return resized;
    }
    function resizeKeepAspectRatioNewHeight(blob, imageWidth, imageHeight, newHeight) {
        if (0 >= imageWidth || 0 >= imageHeight || 0 >= newHeight) return blob;
        var ratio = imageWidth / imageHeight;
        var w = newHeight * ratio;
        var h = newHeight;
        Ti.API.info("ratio: " + ratio);
        Ti.API.info("w: " + w);
        Ti.API.info("h: " + h);
        return ImageFactory.imageAsResized(blob, {
            width: w,
            height: h
        });
    }
    function PagingControl() {
        var pageColor = "#5abf93";
        var container = Titanium.UI.createView({
            height: 60,
            width: Ti.UI.SIZE
        });
        var viewArray = $.recipesList.getViews();
        numberOfPages = viewArray.length;
        pages = [];
        for (var i = 0; numberOfPages > i; i++) {
            page = Titanium.UI.createView({
                borderRadius: 4,
                borderColor: "#fff",
                width: 8,
                height: 8,
                left: 15 * i,
                backgroundColor: pageColor,
                opacity: .3
            });
            pages.push(page);
            container.add(page);
        }
        pages[0].setOpacity(1);
        $.recipesList.addEventListener("scroll", onScroll);
        $.recipesList.addEventListener("postlayout", onPostLayout);
        return container;
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "recipe-listing";
    if (arguments[0]) {
        __processArg(arguments[0], "__parentSymbol");
        __processArg(arguments[0], "$model");
        __processArg(arguments[0], "__itemTemplate");
    }
    var $ = this;
    var exports = {};
    $.__views.recipeListing = Ti.UI.createWindow({
        backgroundImage: "images/grey_nav.png",
        id: "recipeListing",
        backgroundColor: "#ffffff",
        title: "Recipes",
        barColor: "#5abf93"
    });
    $.__views.recipeListing && $.addTopLevelView($.__views.recipeListing);
    $.__views.recipeBackground = Ti.UI.createView({
        id: "recipeBackground",
        width: Ti.UI.FILL,
        height: Ti.UI.FILL
    });
    $.__views.recipeListing.add($.__views.recipeBackground);
    $.__views.__alloyId46 = Ti.UI.createView({
        backgroundColor: "#000000",
        opacity: "0.2",
        width: Ti.UI.FILL,
        height: Ti.UI.FILL,
        id: "__alloyId46"
    });
    $.__views.recipeListing.add($.__views.__alloyId46);
    $.__views.__alloyId47 = Ti.UI.createView({
        backgroundImage: "images/recipes/recipe-lists_bg.png",
        width: "270",
        height: "220",
        top: "20",
        id: "__alloyId47"
    });
    $.__views.recipeListing.add($.__views.__alloyId47);
    var __alloyId48 = [];
    $.__views.recipesList = Ti.UI.createScrollableView({
        views: __alloyId48,
        id: "recipesList",
        showPagingControl: "false",
        pagingControlColor: "#5abf93",
        pagingControlAlpha: "0.9",
        height: "240",
        width: "270",
        top: "0"
    });
    $.__views.__alloyId47.add($.__views.recipesList);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var ImageFactory = require("ti.imagefactory");
    var mod = require("bencoding.blur");
    $.recipeListing.titleAttributes = {
        color: "white",
        font: {
            fontFamily: "Roboto",
            fontSize: 14
        }
    };
    var args = arguments[0] || {};
    $.recipeListing.title = args.data;
    $.recipeListing.navTintColor = "#ffffff";
    Number(Ti.App.Properties.getString("lastReading", "7.0"));
    mod.createBasicBlurView({
        width: Ti.UI.FILL,
        height: Ti.UI.FILL,
        blurRadius: 5
    });
    var db = Ti.Database.open("recipes");
    var recipesRS = db.execute("SELECT id, wpID, title,image, serves,ph_bracket,mealtime FROM recipes WHERE mealtime='" + args.mealtime + "'");
    var index = 0;
    var firstImage = true;
    while (recipesRS.isValidRow()) {
        var recipeID = recipesRS.fieldByName("id");
        var wpID = recipesRS.fieldByName("wpID");
        var title = recipesRS.fieldByName("title");
        var serves = recipesRS.fieldByName("serves");
        var phBracket = recipesRS.fieldByName("ph_bracket");
        var cookingTime = recipesRS.fieldByName("mealtime");
        var thumb = recipesRS.fieldByName("image");
        Ti.API.info("id: " + recipeID + " - title: " + title + " - serves: " + serves);
        var filename = thumb.split("/");
        filename = filename[filename.length - 1];
        var file = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, wpID, filename);
        var blob = file.read();
        Ti.API.info("filename: " + filename + "- blob: " + blob);
        var myview = Ti.UI.createView({
            action: "view1",
            width: 270,
            height: 220,
            top: 0,
            recipeID: recipeID,
            bgImage: blob,
            viewIndex: index,
            zIndex: 10
        });
        if (void 0 != blob) {
            if (firstImage) {
                var blurView = mod.createBasicBlurView({
                    width: Ti.UI.FILL,
                    height: Ti.UI.FILL,
                    blurLevel: 8,
                    blurTintColor: "#000000",
                    blurRadius: 5,
                    image: blob
                });
                $.recipeBackground.add(blurView);
                firstImage = false;
            }
            var blob2 = resizeKeepAspectRatioNewWidth(blob, blob.width, blob.height, 220);
            var blob3 = resizeKeepAspectRatioNewHeight(blob, blob.width, blob.height, 548);
            var myImage = Ti.UI.createMaskedImage({
                action: "view2",
                mask: "images/recipeListMask.png",
                image: blob2,
                originalBlob: blob3,
                width: 270,
                height: 180,
                zIndex: 11,
                top: 0,
                touchEnabled: false
            });
            myview.add(myImage);
        }
        var labelCont = Ti.UI.createView({
            action: "view3",
            width: Ti.UI.FILL,
            height: "35",
            backgroundColor: "#000000",
            opacity: .5,
            touchEnabled: false,
            bottom: 39,
            zIndex: 12
        });
        var label = Ti.UI.createLabel({
            textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
            width: 200,
            height: 35,
            left: 5,
            text: title,
            action: "label",
            touchEnabled: false,
            font: {
                fontFamily: "Roboto",
                fontSize: "13"
            },
            color: "#fff"
        });
        labelCont.add(label);
        myview.add(labelCont);
        var phCont = Ti.UI.createView({
            width: "70",
            height: "35",
            backgroundColor: "#6cc19c",
            touchEnabled: false,
            bottom: 39,
            right: 0,
            zIndex: 12
        });
        var phIcon = Ti.UI.createImageView({
            image: "images/ph_icon.png",
            width: 12,
            heigth: 15,
            left: 3
        });
        var prePhLabel = Ti.UI.createLabel({
            textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
            width: 15,
            height: 35,
            left: 18,
            text: "pH",
            action: "label",
            touchEnabled: false,
            font: {
                fontFamily: "Roboto Condensed",
                fontSize: "10"
            },
            color: "#fff"
        });
        var phLabel = Ti.UI.createLabel({
            textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
            width: 60,
            height: 35,
            left: 30,
            text: phBracket,
            action: "label",
            touchEnabled: false,
            font: {
                fontFamily: "Roboto Light",
                fontSize: "10"
            },
            color: "#fff"
        });
        phCont.add(phIcon);
        phCont.add(prePhLabel);
        phCont.add(phLabel);
        myview.add(phCont);
        var cookingTimeLabel = Ti.UI.createLabel({
            textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
            width: 100,
            height: 35,
            left: 32,
            bottom: 2,
            text: cookingTime + " minutes",
            action: "label",
            touchEnabled: false,
            font: {
                fontFamily: "Roboto Light",
                fontSize: "13"
            },
            color: "#818385"
        });
        myview.add(cookingTimeLabel);
        var servesLabel = Ti.UI.createLabel({
            textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
            width: 100,
            height: 35,
            left: 162,
            bottom: 2,
            text: serves + " people",
            action: "label",
            touchEnabled: false,
            font: {
                fontFamily: "Roboto Light",
                fontSize: "13"
            },
            color: "#818385"
        });
        myview.add(servesLabel);
        var plus = Ti.UI.createImageView({
            width: 20,
            height: 19,
            left: 237,
            image: "images/plus_icon.png",
            zIndex: 14
        });
        myview.add(plus);
        $.recipesList.addView(myview);
        myview.addEventListener("click", openDetails);
        index++;
        recipesRS.next();
    }
    recipesRS.close();
    db.close();
    $.recipesList.addEventListener("scrollend", function(e) {
        Ti.API.info("index: " + e.currentPage);
        var viewArray = $.recipesList.getViews();
        Ti.API.info("view: " + viewArray[e.currentPage].children[0].action);
        var blurView = mod.createBasicBlurView({
            width: viewArray[e.currentPage].children[0].originalBlob.width,
            height: viewArray[e.currentPage].children[0].originalBlob.height,
            blurLevel: 5,
            blurTintColor: "#000000",
            blurRadius: 5,
            image: viewArray[e.currentPage].children[0].originalBlob
        });
        $.recipeBackground.add(blurView);
    });
    var pages = [];
    var page;
    var numberOfPages = 0;
    onScroll = function(event) {
        for (var i = 0; numberOfPages > i; i++) pages[i].setOpacity(.3);
        pages[event.currentPage].setOpacity(1);
    };
    onPostLayout = function() {
        for (var i = 0; numberOfPages > i; i++) pages[i].setOpacity(.3);
        pages[$.recipesList.currentPage].setOpacity(1);
    };
    var mypaging = PagingControl($.recipesList);
    $.recipeListing.add(mypaging);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;