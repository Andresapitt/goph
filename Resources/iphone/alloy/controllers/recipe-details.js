function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "recipe-details";
    if (arguments[0]) {
        __processArg(arguments[0], "__parentSymbol");
        __processArg(arguments[0], "$model");
        __processArg(arguments[0], "__itemTemplate");
    }
    var $ = this;
    var exports = {};
    $.__views.recipeDetails = Ti.UI.createWindow({
        id: "recipeDetails",
        backgroundColor: "#ffffff",
        barColor: "#5abf93",
        backgroundImage: "images/main_bg.png",
        title: "..."
    });
    $.__views.recipeDetails && $.addTopLevelView($.__views.recipeDetails);
    $.__views.recipeCircle = Ti.UI.createMaskedImage({
        id: "recipeCircle",
        mask: "images/circleMask.png",
        width: "73",
        height: "73",
        left: "25",
        top: "15"
    });
    $.__views.recipeDetails.add($.__views.recipeCircle);
    $.__views.__alloyId37 = Ti.UI.createImageView({
        image: "images/white_border.png",
        width: "73",
        height: "73",
        left: "25",
        top: "15",
        id: "__alloyId37"
    });
    $.__views.recipeDetails.add($.__views.__alloyId37);
    $.__views.recipeLabel = Ti.UI.createLabel({
        color: "#fff",
        font: {
            fontFamily: "Roboto",
            fontSize: "18dp",
            fontStyle: "bold",
            fontWeight: "bold"
        },
        shadowColor: "#000",
        shadowOffset: {
            x: 1,
            y: 1
        },
        shadowRadius: 2,
        id: "recipeLabel",
        top: "40",
        width: "193",
        height: "30",
        left: "103"
    });
    $.__views.recipeDetails.add($.__views.recipeLabel);
    $.__views.__alloyId38 = Ti.UI.createView({
        width: Ti.UI.SIZE,
        height: "11",
        top: "65",
        left: "103",
        id: "__alloyId38"
    });
    $.__views.recipeDetails.add($.__views.__alloyId38);
    $.__views.__alloyId39 = Ti.UI.createImageView({
        backgroundImage: "images/recipe_icons.png",
        width: "136",
        height: "11",
        left: "0",
        id: "__alloyId39"
    });
    $.__views.__alloyId38.add($.__views.__alloyId39);
    $.__views.preparationTimeLabel = Ti.UI.createLabel({
        color: "#fff",
        font: {
            fontFamily: "Roboto Light",
            fontSize: "10dp"
        },
        text: "15 minutes",
        id: "preparationTimeLabel",
        left: "13"
    });
    $.__views.__alloyId38.add($.__views.preparationTimeLabel);
    $.__views.servesLabel = Ti.UI.createLabel({
        color: "#fff",
        font: {
            fontFamily: "Roboto Light",
            fontSize: "10dp"
        },
        text: "2 people",
        id: "servesLabel",
        left: "80"
    });
    $.__views.__alloyId38.add($.__views.servesLabel);
    $.__views.phInformationLabel = Ti.UI.createLabel({
        color: "#fff",
        font: {
            fontFamily: "Roboto Light",
            fontSize: "10dp"
        },
        text: "7.2 to 7.6",
        id: "phInformationLabel",
        left: "139"
    });
    $.__views.__alloyId38.add($.__views.phInformationLabel);
    $.__views.__alloyId40 = Ti.UI.createView({
        width: "270",
        height: "405",
        bottom: "0",
        id: "__alloyId40"
    });
    $.__views.recipeDetails.add($.__views.__alloyId40);
    $.__views.__alloyId41 = Ti.UI.createView({
        width: Ti.UI.FILL,
        height: Ti.UI.FILL,
        backgroundColor: "#000000",
        opacity: "0.5",
        id: "__alloyId41"
    });
    $.__views.__alloyId40.add($.__views.__alloyId41);
    $.__views.phInfoBtn = Ti.UI.createView({
        id: "phInfoBtn",
        left: "0",
        width: "90",
        height: "35",
        top: "10",
        zIndex: "12"
    });
    $.__views.__alloyId40.add($.__views.phInfoBtn);
    $.__views.phInfoBtnLabel = Ti.UI.createLabel({
        color: "#fff",
        font: {
            fontFamily: "Roboto",
            fontSize: "12dp",
            fontStyle: "bold",
            fontWeight: "bold"
        },
        text: "pH Information",
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        width: Ti.UI.FILL,
        id: "phInfoBtnLabel"
    });
    $.__views.phInfoBtn.add($.__views.phInfoBtnLabel);
    $.__views.__alloyId42 = Ti.UI.createView({
        width: "1",
        height: "35",
        top: "10",
        left: "90",
        backgroundColor: "#ffffff",
        opacity: "0.6",
        id: "__alloyId42"
    });
    $.__views.__alloyId40.add($.__views.__alloyId42);
    $.__views.ingredientsBtn = Ti.UI.createView({
        id: "ingredientsBtn",
        left: "90",
        width: "90",
        height: "35",
        top: "10",
        zIndex: "11"
    });
    $.__views.__alloyId40.add($.__views.ingredientsBtn);
    $.__views.ingredientsBtnLabel = Ti.UI.createLabel({
        color: "#fff",
        font: {
            fontFamily: "Roboto",
            fontSize: "12dp",
            fontStyle: "bold",
            fontWeight: "bold"
        },
        text: "Ingredients",
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        width: Ti.UI.FILL,
        id: "ingredientsBtnLabel"
    });
    $.__views.ingredientsBtn.add($.__views.ingredientsBtnLabel);
    $.__views.__alloyId43 = Ti.UI.createView({
        width: "1",
        height: "35",
        top: "10",
        left: "180",
        backgroundColor: "#ffffff",
        opacity: "0.6",
        id: "__alloyId43"
    });
    $.__views.__alloyId40.add($.__views.__alloyId43);
    $.__views.preparationBtn = Ti.UI.createView({
        id: "preparationBtn",
        left: "180",
        width: "90",
        height: "35",
        top: "10",
        zIndex: "12"
    });
    $.__views.__alloyId40.add($.__views.preparationBtn);
    $.__views.preparationBtnLabel = Ti.UI.createLabel({
        color: "#fff",
        font: {
            fontFamily: "Roboto",
            fontSize: "12dp",
            fontStyle: "bold",
            fontWeight: "bold"
        },
        text: "Preparation",
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        width: Ti.UI.FILL,
        id: "preparationBtnLabel"
    });
    $.__views.preparationBtn.add($.__views.preparationBtnLabel);
    $.__views.__alloyId44 = Ti.UI.createView({
        width: "80%",
        height: "1",
        backgroundColor: "#ffffff",
        top: "55",
        opacity: "0.6",
        id: "__alloyId44"
    });
    $.__views.__alloyId40.add($.__views.__alloyId44);
    var __alloyId45 = [];
    $.__views.scrollablePhInfo = Ti.UI.createView({
        id: "scrollablePhInfo"
    });
    __alloyId45.push($.__views.scrollablePhInfo);
    $.__views.scrollView = Ti.UI.createScrollView({
        id: "scrollView",
        showVerticalScrollIndicator: "true",
        top: "0",
        height: Ti.UI.SIZE,
        width: Ti.UI.FILL
    });
    $.__views.scrollablePhInfo.add($.__views.scrollView);
    $.__views.phInfoText = Ti.UI.createLabel({
        color: "#fff",
        font: {
            fontFamily: "Roboto Light",
            fontSize: "13dp"
        },
        text: "Augue ultrices aenean natoque, ridiculus scelerisque porttitor sit phasellus platea, scelerisque, massa sociis turpis in sed eu non. Turpis, et. Tortor sit pellentesque sed a amet augue? Auctor, quis a! Scelerisque? Nunc pid porta. Parturient quis! Sed in! Porttitor tristique habitasse turpis placerat adipiscing et tortor porta nunc vel, augue cursus in, scelerisque est in dis, turpis lectus dis rhoncus? Sociis adipiscing vel, quis velit sit vel! Rhoncus? Platea lorem amet montes! Penatibus scelerisque mid? Augue. Egestas ut quis porttitor non. Phasellus dictumst velit elit turpis porttitor lectus sed mus cras! Phasellus, placerat, vel tincidunt proin, porttitor et? Elit a, magna platea? Diam velit enim ridiculus, sed, et, lacus ac ultrices cum in, amet facilisis porta augue massa non augue.\n					\n					Scelerisque, et rhoncus tincidunt! Mauris magna nascetur. Pulvinar et odio ut urna vut sociis ut massa facilisis turpis lorem mus porttitor hac habitasse diam et mus, et, dictumst porta arcu tempor tortor ac! Tincidunt cum, odio tincidunt, integer proin vel a tincidunt a ultrices penatibus etiam facilisis tempor habitasse, enim? Nisi quis lorem, pulvinar pulvinar! Amet pulvinar arcu, eu? Eros ac, ac enim odio? Amet elementum, eros! Sed urna cum porta, velit adipiscing turpis ridiculus, ut adipiscing eros cras tempor enim lacus nisi, mid augue, nec, porta, nascetur mus? Pulvinar nec diam tincidunt duis egestas. Integer in amet phasellus! Porttitor magna mus augue purus vel vut amet ut tincidunt dignissim! A proin magnis, nunc diam, augue placerat! Porttitor aenean.",
        id: "phInfoText",
        top: "0",
        width: "90%",
        height: Ti.UI.SIZE
    });
    $.__views.scrollView.add($.__views.phInfoText);
    $.__views.scrollableIngredients = Ti.UI.createView({
        id: "scrollableIngredients"
    });
    __alloyId45.push($.__views.scrollableIngredients);
    $.__views.scrollView = Ti.UI.createScrollView({
        id: "scrollView",
        showVerticalScrollIndicator: "true",
        top: "0",
        height: Ti.UI.SIZE,
        width: Ti.UI.FILL
    });
    $.__views.scrollableIngredients.add($.__views.scrollView);
    $.__views.ingredientsText = Ti.UI.createLabel({
        color: "#fff",
        font: {
            fontFamily: "Roboto Light",
            fontSize: "13dp"
        },
        text: "Augue ultrices aenean natoque, ridiculus scelerisque porttitor sit phasellus platea, scelerisque, massa sociis turpis in sed eu non. Turpis, et. Tortor sit pellentesque sed a amet augue? Auctor, quis a! Scelerisque? Nunc pid porta. Parturient quis! Sed in! Porttitor tristique habitasse turpis placerat adipiscing et tortor porta nunc vel, augue cursus in, scelerisque est in dis, turpis lectus dis rhoncus? Sociis adipiscing vel, quis velit sit vel! Rhoncus? Platea lorem amet montes! Penatibus scelerisque mid? Augue. Egestas ut quis porttitor non. Phasellus dictumst velit elit turpis porttitor lectus sed mus cras! Phasellus, placerat, vel tincidunt proin, porttitor et? Elit a, magna platea? Diam velit enim ridiculus, sed, et, lacus ac ultrices cum in, amet facilisis porta augue massa non augue.\n					\n					Scelerisque, et rhoncus tincidunt! Mauris magna nascetur. Pulvinar et odio ut urna vut sociis ut massa facilisis turpis lorem mus porttitor hac habitasse diam et mus, et, dictumst porta arcu tempor tortor ac! Tincidunt cum, odio tincidunt, integer proin vel a tincidunt a ultrices penatibus etiam facilisis tempor habitasse, enim? Nisi quis lorem, pulvinar pulvinar! Amet pulvinar arcu, eu? Eros ac, ac enim odio? Amet elementum, eros! Sed urna cum porta, velit adipiscing turpis ridiculus, ut adipiscing eros cras tempor enim lacus nisi, mid augue, nec, porta, nascetur mus? Pulvinar nec diam tincidunt duis egestas. Integer in amet phasellus! Porttitor magna mus augue purus vel vut amet ut tincidunt dignissim! A proin magnis, nunc diam, augue placerat! Porttitor aenean.",
        id: "ingredientsText",
        top: "0",
        width: "90%",
        height: "340"
    });
    $.__views.scrollView.add($.__views.ingredientsText);
    $.__views.scrollablePreparation = Ti.UI.createView({
        id: "scrollablePreparation"
    });
    __alloyId45.push($.__views.scrollablePreparation);
    $.__views.scrollView = Ti.UI.createScrollView({
        id: "scrollView",
        showVerticalScrollIndicator: "true",
        top: "0",
        height: Ti.UI.SIZE,
        width: Ti.UI.FILL
    });
    $.__views.scrollablePreparation.add($.__views.scrollView);
    $.__views.preparationText = Ti.UI.createLabel({
        color: "#fff",
        font: {
            fontFamily: "Roboto Light",
            fontSize: "13dp"
        },
        text: "Augue ultrices aenean natoque, ridiculus scelerisque porttitor sit phasellus platea, scelerisque, massa sociis turpis in sed eu non. Turpis, et. Tortor sit pellentesque sed a amet augue? Auctor, quis a! Scelerisque? Nunc pid porta. Parturient quis! Sed in! Porttitor tristique habitasse turpis placerat adipiscing et tortor porta nunc vel, augue cursus in, scelerisque est in dis, turpis lectus dis rhoncus? Sociis adipiscing vel, quis velit sit vel! Rhoncus? Platea lorem amet montes! Penatibus scelerisque mid? Augue. Egestas ut quis porttitor non. Phasellus dictumst velit elit turpis porttitor lectus sed mus cras! Phasellus, placerat, vel tincidunt proin, porttitor et? Elit a, magna platea? Diam velit enim ridiculus, sed, et, lacus ac ultrices cum in, amet facilisis porta augue massa non augue.\n					\n					Scelerisque, et rhoncus tincidunt! Mauris magna nascetur. Pulvinar et odio ut urna vut sociis ut massa facilisis turpis lorem mus porttitor hac habitasse diam et mus, et, dictumst porta arcu tempor tortor ac! Tincidunt cum, odio tincidunt, integer proin vel a tincidunt a ultrices penatibus etiam facilisis tempor habitasse, enim? Nisi quis lorem, pulvinar pulvinar! Amet pulvinar arcu, eu? Eros ac, ac enim odio? Amet elementum, eros! Sed urna cum porta, velit adipiscing turpis ridiculus, ut adipiscing eros cras tempor enim lacus nisi, mid augue, nec, porta, nascetur mus? Pulvinar nec diam tincidunt duis egestas. Integer in amet phasellus! Porttitor magna mus augue purus vel vut amet ut tincidunt dignissim! A proin magnis, nunc diam, augue placerat! Porttitor aenean.",
        id: "preparationText",
        top: "0",
        width: "90%",
        height: "340"
    });
    $.__views.scrollView.add($.__views.preparationText);
    $.__views.recipeDetailsScrollableView = Ti.UI.createScrollableView({
        views: __alloyId45,
        id: "recipeDetailsScrollableView",
        showPagingControl: "false",
        top: "65",
        height: Ti.UI.SIZE,
        width: "270"
    });
    $.__views.__alloyId40.add($.__views.recipeDetailsScrollableView);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var ImageFactory = require("ti.imagefactory");
    $.recipeDetails.titleAttributes = {
        color: "white",
        font: {
            fontFamily: "Roboto",
            fontSize: 14
        }
    };
    $.recipeDetails.navTintColor = "#ffffff";
    var args = arguments[0] || {};
    $.recipeDetails.title = args.data;
    $.recipeDetails.navTintColor = "#ffffff";
    var iPhone5 = 568 === Ti.Platform.displayCaps.platformHeight;
    iPhone5 && ($.scrollView.height = 180);
    var db = Ti.Database.open("recipes");
    var recipesRS = db.execute("SELECT id,wpID, ph_information,preparation, ingredients, title, serves, image FROM recipes WHERE id=" + args.recipeID);
    if (recipesRS.isValidRow()) {
        $.recipeLabel.text = recipesRS.fieldByName("title");
        Ti.API.info("ph info for Id:" + args.recipeID + " - " + recipesRS.fieldByName("ph_information"));
        $.phInfoText.text = recipesRS.fieldByName("ph_information");
        $.ingredientsText.text = recipesRS.fieldByName("ingredients");
        $.preparationText.text = recipesRS.fieldByName("preparation");
        var wpID = recipesRS.fieldByName("wpID");
        var image = recipesRS.fieldByName("image");
        var wpID = recipesRS.fieldByName("wpID");
        var filename = image.split("/");
        filename = filename[filename.length - 1];
        var file = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, wpID, filename);
        var blob = file.read();
        if (void 0 != blob) {
            var blob2 = ImageFactory.imageAsThumbnail(blob, {
                size: 74,
                quality: ImageFactory.QUALITY_HIGH
            });
            $.recipeCircle.image = blob2;
        }
    }
    recipesRS.close();
    db.close();
    $.phInfoBtn.addEventListener("click", function() {
        $.recipeDetailsScrollableView.scrollToView(0);
        $.phInfoBtnLabel.color = "#5abf93";
        $.preparationBtnLabel.color = "#FFFFFF";
        $.ingredientsBtnLabel.color = "#FFFFFF";
    });
    $.ingredientsBtn.addEventListener("click", function() {
        $.recipeDetailsScrollableView.scrollToView(1);
        $.phInfoBtnLabel.color = "#FFFFFF";
        $.preparationBtnLabel.color = "#FFFFFF";
        $.ingredientsBtnLabel.color = "#5abf93";
    });
    $.preparationBtn.addEventListener("click", function() {
        $.recipeDetailsScrollableView.scrollToView(2);
        $.phInfoBtnLabel.color = "#FFFFFF";
        $.preparationBtnLabel.color = "#5abf93";
        $.ingredientsBtnLabel.color = "#FFFFFF";
    });
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;