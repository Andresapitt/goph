function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "meals-listing";
    if (arguments[0]) {
        __processArg(arguments[0], "__parentSymbol");
        __processArg(arguments[0], "$model");
        __processArg(arguments[0], "__itemTemplate");
    }
    var $ = this;
    var exports = {};
    $.__views.mealListing = Ti.UI.createWindow({
        backgroundImage: "images/grey_nav.png",
        id: "mealListing",
        backgroundColor: "#ffffff",
        layout: "vertical",
        title: "Recipes"
    });
    $.__views.mealListing && $.addTopLevelView($.__views.mealListing);
    $.__views.lastRecordedLabel = Ti.UI.createLabel({
        color: "#ffffff",
        font: {
            fontFamily: "Helvetica",
            fontSize: "15dp",
            fontStyle: "normal",
            fontWeight: "normal"
        },
        text: "your last recorded value was:",
        id: "lastRecordedLabel",
        top: "0",
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        width: "100%",
        height: "30"
    });
    $.__views.mealListing.add($.__views.lastRecordedLabel);
    var __alloyId8 = [];
    var __alloyId9 = {
        color: "#ffffff",
        backgroundColor: "#c9a037",
        font: {
            fontFamily: "Helvetica",
            fontSize: "14dp",
            fontStyle: "normal",
            fontWeight: "normal"
        },
        title: "5 - 6.5"
    };
    __alloyId8.push(__alloyId9);
    var __alloyId10 = {
        color: "#ffffff",
        backgroundColor: "#225350",
        font: {
            fontFamily: "Helvetica",
            fontSize: "14dp",
            fontStyle: "normal",
            fontWeight: "normal"
        },
        title: "6.6 - 7.5"
    };
    __alloyId8.push(__alloyId10);
    var __alloyId11 = {
        color: "#ffffff",
        backgroundColor: "#023867",
        font: {
            fontFamily: "Helvetica",
            fontSize: "14dp",
            fontStyle: "normal",
            fontWeight: "normal"
        },
        title: "7.6 - 8.5"
    };
    __alloyId8.push(__alloyId11);
    $.__views.levelsTab = Ti.UI.iOS.createTabbedBar({
        labels: __alloyId8,
        id: "levelsTab",
        backgroundColor: "#369",
        top: "10",
        height: "25",
        width: "200"
    });
    $.__views.mealListing.add($.__views.levelsTab);
    $.__views.scrollView = Ti.UI.createScrollView({
        id: "scrollView",
        showVerticalScrollIndicator: "true",
        top: "8",
        height: "450",
        contentHeight: "700",
        width: "90%",
        layout: "vertical"
    });
    $.__views.mealListing.add($.__views.scrollView);
    $.__views.__alloyId12 = Ti.UI.createView({
        height: Ti.UI.SIZE,
        top: "5",
        id: "__alloyId12"
    });
    $.__views.scrollView.add($.__views.__alloyId12);
    $.__views.view = Ti.UI.createView({
        backgroundColor: "#c4c4c4",
        id: "view",
        borderRadius: "10",
        left: "10",
        width: "120",
        height: "80"
    });
    $.__views.__alloyId12.add($.__views.view);
    $.__views.__alloyId13 = Ti.UI.createLabel({
        color: "#ffffff",
        font: {
            fontFamily: "Helvetica",
            fontSize: "13dp",
            fontStyle: "normal",
            fontWeight: "normal"
        },
        text: "recipe one",
        id: "__alloyId13"
    });
    $.__views.view.add($.__views.__alloyId13);
    $.__views.view2 = Ti.UI.createView({
        backgroundColor: "#c4c4c4",
        id: "view2",
        borderRadius: "10",
        right: "10",
        width: "120",
        height: "80"
    });
    $.__views.__alloyId12.add($.__views.view2);
    $.__views.__alloyId14 = Ti.UI.createLabel({
        color: "#ffffff",
        font: {
            fontFamily: "Helvetica",
            fontSize: "13dp",
            fontStyle: "normal",
            fontWeight: "normal"
        },
        text: "recipe two",
        id: "__alloyId14"
    });
    $.__views.view2.add($.__views.__alloyId14);
    $.__views.__alloyId15 = Ti.UI.createView({
        height: Ti.UI.SIZE,
        top: "15",
        id: "__alloyId15"
    });
    $.__views.scrollView.add($.__views.__alloyId15);
    $.__views.view3 = Ti.UI.createView({
        backgroundColor: "#c4c4c4",
        id: "view3",
        borderRadius: "10",
        left: "10",
        width: "120",
        height: "80"
    });
    $.__views.__alloyId15.add($.__views.view3);
    $.__views.__alloyId16 = Ti.UI.createLabel({
        color: "#ffffff",
        font: {
            fontFamily: "Helvetica",
            fontSize: "13dp",
            fontStyle: "normal",
            fontWeight: "normal"
        },
        text: "recipe three",
        id: "__alloyId16"
    });
    $.__views.view3.add($.__views.__alloyId16);
    $.__views.view4 = Ti.UI.createView({
        backgroundColor: "#c4c4c4",
        id: "view4",
        borderRadius: "10",
        right: "10",
        width: "120",
        height: "80"
    });
    $.__views.__alloyId15.add($.__views.view4);
    $.__views.__alloyId17 = Ti.UI.createLabel({
        color: "#ffffff",
        font: {
            fontFamily: "Helvetica",
            fontSize: "13dp",
            fontStyle: "normal",
            fontWeight: "normal"
        },
        text: "recipe four",
        id: "__alloyId17"
    });
    $.__views.view4.add($.__views.__alloyId17);
    $.__views.__alloyId18 = Ti.UI.createView({
        height: Ti.UI.SIZE,
        top: "15",
        id: "__alloyId18"
    });
    $.__views.scrollView.add($.__views.__alloyId18);
    $.__views.view5 = Ti.UI.createView({
        backgroundColor: "#c4c4c4",
        id: "view5",
        borderRadius: "10",
        left: "10",
        width: "120",
        height: "80"
    });
    $.__views.__alloyId18.add($.__views.view5);
    $.__views.__alloyId19 = Ti.UI.createLabel({
        color: "#ffffff",
        font: {
            fontFamily: "Helvetica",
            fontSize: "13dp",
            fontStyle: "normal",
            fontWeight: "normal"
        },
        text: "recipe five",
        id: "__alloyId19"
    });
    $.__views.view5.add($.__views.__alloyId19);
    $.__views.view6 = Ti.UI.createView({
        backgroundColor: "#c4c4c4",
        id: "view6",
        borderRadius: "10",
        right: "10",
        width: "120",
        height: "80"
    });
    $.__views.__alloyId18.add($.__views.view6);
    $.__views.__alloyId20 = Ti.UI.createLabel({
        color: "#ffffff",
        font: {
            fontFamily: "Helvetica",
            fontSize: "13dp",
            fontStyle: "normal",
            fontWeight: "normal"
        },
        text: "recipe six",
        id: "__alloyId20"
    });
    $.__views.view6.add($.__views.__alloyId20);
    $.__views.__alloyId21 = Ti.UI.createView({
        height: Ti.UI.SIZE,
        top: "15",
        id: "__alloyId21"
    });
    $.__views.scrollView.add($.__views.__alloyId21);
    $.__views.view7 = Ti.UI.createView({
        backgroundColor: "#c4c4c4",
        id: "view7",
        borderRadius: "10",
        left: "10",
        width: "120",
        height: "80"
    });
    $.__views.__alloyId21.add($.__views.view7);
    $.__views.__alloyId22 = Ti.UI.createLabel({
        color: "#ffffff",
        font: {
            fontFamily: "Helvetica",
            fontSize: "13dp",
            fontStyle: "normal",
            fontWeight: "normal"
        },
        text: "recipe seven",
        id: "__alloyId22"
    });
    $.__views.view7.add($.__views.__alloyId22);
    $.__views.view8 = Ti.UI.createView({
        backgroundColor: "#c4c4c4",
        id: "view8",
        borderRadius: "10",
        right: "10",
        width: "120",
        height: "80"
    });
    $.__views.__alloyId21.add($.__views.view8);
    $.__views.__alloyId23 = Ti.UI.createLabel({
        color: "#ffffff",
        font: {
            fontFamily: "Helvetica",
            fontSize: "13dp",
            fontStyle: "normal",
            fontWeight: "normal"
        },
        text: "recipe eight",
        id: "__alloyId23"
    });
    $.__views.view8.add($.__views.__alloyId23);
    $.__views.__alloyId24 = Ti.UI.createView({
        height: Ti.UI.SIZE,
        top: "15",
        id: "__alloyId24"
    });
    $.__views.scrollView.add($.__views.__alloyId24);
    $.__views.view9 = Ti.UI.createView({
        backgroundColor: "#c4c4c4",
        id: "view9",
        borderRadius: "10",
        left: "10",
        width: "120",
        height: "80"
    });
    $.__views.__alloyId24.add($.__views.view9);
    $.__views.__alloyId25 = Ti.UI.createLabel({
        color: "#ffffff",
        font: {
            fontFamily: "Helvetica",
            fontSize: "13dp",
            fontStyle: "normal",
            fontWeight: "normal"
        },
        text: "recipe nine",
        id: "__alloyId25"
    });
    $.__views.view9.add($.__views.__alloyId25);
    $.__views.view10 = Ti.UI.createView({
        backgroundColor: "#c4c4c4",
        id: "view10",
        borderRadius: "10",
        right: "10",
        width: "120",
        height: "80"
    });
    $.__views.__alloyId24.add($.__views.view10);
    $.__views.__alloyId26 = Ti.UI.createLabel({
        color: "#ffffff",
        font: {
            fontFamily: "Helvetica",
            fontSize: "13dp",
            fontStyle: "normal",
            fontWeight: "normal"
        },
        text: "recipe ten",
        id: "__alloyId26"
    });
    $.__views.view10.add($.__views.__alloyId26);
    $.__views.__alloyId27 = Ti.UI.createView({
        height: Ti.UI.SIZE,
        top: "15",
        id: "__alloyId27"
    });
    $.__views.scrollView.add($.__views.__alloyId27);
    $.__views.view11 = Ti.UI.createView({
        backgroundColor: "#c4c4c4",
        id: "view11",
        borderRadius: "10",
        left: "10",
        width: "120",
        height: "80"
    });
    $.__views.__alloyId27.add($.__views.view11);
    $.__views.__alloyId28 = Ti.UI.createLabel({
        color: "#ffffff",
        font: {
            fontFamily: "Helvetica",
            fontSize: "13dp",
            fontStyle: "normal",
            fontWeight: "normal"
        },
        text: "recipe eleven",
        id: "__alloyId28"
    });
    $.__views.view11.add($.__views.__alloyId28);
    $.__views.view12 = Ti.UI.createView({
        backgroundColor: "#c4c4c4",
        id: "view12",
        borderRadius: "10",
        right: "10",
        width: "120",
        height: "80"
    });
    $.__views.__alloyId27.add($.__views.view12);
    $.__views.__alloyId29 = Ti.UI.createLabel({
        color: "#ffffff",
        font: {
            fontFamily: "Helvetica",
            fontSize: "13dp",
            fontStyle: "normal",
            fontWeight: "normal"
        },
        text: "recipe twelve",
        id: "__alloyId29"
    });
    $.__views.view12.add($.__views.__alloyId29);
    exports.destroy = function() {};
    _.extend($, $.__views);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;