function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "times-of-day";
    if (arguments[0]) {
        __processArg(arguments[0], "__parentSymbol");
        __processArg(arguments[0], "$model");
        __processArg(arguments[0], "__itemTemplate");
    }
    var $ = this;
    var exports = {};
    $.__views.timeOfDay = Ti.UI.createWindow({
        backgroundImage: "images/grey_nav.png",
        id: "timeOfDay",
        barColor: "#5abf93",
        backgroundColor: "#ffffff",
        title: "Times of Day"
    });
    $.__views.timeOfDay && $.addTopLevelView($.__views.timeOfDay);
    $.__views.breakfast = Ti.UI.createImageView({
        id: "breakfast",
        image: "/images/recipes/mealtimes_breakfast.jpg",
        top: "0",
        width: "320",
        height: "126",
        zIndex: "10"
    });
    $.__views.timeOfDay.add($.__views.breakfast);
    $.__views.lunch = Ti.UI.createImageView({
        id: "lunch",
        image: "/images/recipes/mealtimes_lunch.jpg",
        top: "126",
        width: "320",
        height: "126",
        zIndex: "11"
    });
    $.__views.timeOfDay.add($.__views.lunch);
    $.__views.dinner = Ti.UI.createImageView({
        id: "dinner",
        image: "/images/recipes/mealtimes_dinner.jpg",
        top: "252",
        width: "320",
        height: "126",
        zIndex: "12"
    });
    $.__views.timeOfDay.add($.__views.dinner);
    $.__views.snacks = Ti.UI.createImageView({
        id: "snacks",
        image: "/images/recipes/mealtimes_snacks.jpg",
        top: "378",
        width: "320",
        height: "126",
        zIndex: "13"
    });
    $.__views.timeOfDay.add($.__views.snacks);
    $.__views.__alloyId49 = Ti.UI.createView({
        height: "1",
        width: "320",
        top: "126",
        zIndex: "14",
        backgroundColor: "#fff",
        id: "__alloyId49"
    });
    $.__views.timeOfDay.add($.__views.__alloyId49);
    $.__views.__alloyId50 = Ti.UI.createView({
        height: "1",
        width: "320",
        top: "252",
        zIndex: "15",
        backgroundColor: "#fff",
        id: "__alloyId50"
    });
    $.__views.timeOfDay.add($.__views.__alloyId50);
    $.__views.__alloyId51 = Ti.UI.createView({
        height: "1",
        width: "320",
        top: "378",
        zIndex: "16",
        backgroundColor: "#fff",
        id: "__alloyId51"
    });
    $.__views.timeOfDay.add($.__views.__alloyId51);
    exports.destroy = function() {};
    _.extend($, $.__views);
    $.timeOfDay.titleAttributes = {
        color: "white",
        font: {
            fontFamily: "Roboto",
            fontSize: 14
        }
    };
    $.timeOfDay.navTintColor = "#ffffff";
    arguments[0] || {};
    Number(Ti.App.Properties.getString("lastReading", "7.0"));
    $.breakfast.addEventListener("click", function() {
        var args = {
            data: "Breakfast",
            mealtime: 4
        };
        var winRecipeListing = Alloy.createController("recipe-listing", args).getView();
        Alloy.Globals.navGroup.openWindow(winRecipeListing);
    });
    $.lunch.addEventListener("click", function() {
        var args = {
            data: "Lunch",
            mealtime: 5
        };
        var winRecipeListing = Alloy.createController("recipe-listing", args).getView();
        Alloy.Globals.navGroup.openWindow(winRecipeListing);
    });
    $.dinner.addEventListener("click", function() {
        var args = {
            data: "Dinner",
            mealtime: 6
        };
        var winRecipeListing = Alloy.createController("recipe-listing", args).getView();
        Alloy.Globals.navGroup.openWindow(winRecipeListing);
    });
    $.snacks.addEventListener("click", function() {
        var args = {
            data: "Snacks",
            mealtime: 7
        };
        var winRecipeListing = Alloy.createController("recipe-listing", args).getView();
        Alloy.Globals.navGroup.openWindow(winRecipeListing);
    });
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;