function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "alkaline-info";
    if (arguments[0]) {
        __processArg(arguments[0], "__parentSymbol");
        __processArg(arguments[0], "$model");
        __processArg(arguments[0], "__itemTemplate");
    }
    var $ = this;
    var exports = {};
    $.__views.alkalineInfo = Ti.UI.createWindow({
        id: "alkalineInfo",
        backgroundColor: "#ffffff",
        barColor: "#333333",
        backgroundImage: "images/main_bg.png",
        title: "Alkaline",
        titleControl: "#ffffff",
        layout: "vertical"
    });
    $.__views.alkalineInfo && $.addTopLevelView($.__views.alkalineInfo);
    $.__views.scrollView = Ti.UI.createScrollView({
        id: "scrollView",
        showVerticalScrollIndicator: "true",
        top: "10",
        height: Ti.UI.FILL,
        width: "90%"
    });
    $.__views.alkalineInfo.add($.__views.scrollView);
    $.__views.mainText = Ti.UI.createLabel({
        text: "Your results indicate that your body system is in an alkaline state. If your reading in this range tops pH 8 it is indicative of a diet with excess alkaline substances usually as a result of taking alkaline mineral supplements. The danger here is that these supplements are surplus to requirements or your system is being deprived of protein. Maintaining an alkaline acid balance is essential to good health but making extreme dietary changes without medical advice can be counterproductive. It is strongly recommended that if your readings are consistently above pH 8 medical advice is necessary. The recipes in this range are designed to include protein in the form of meat or fish so that the recommended 80 percent alkaline forming food to 20 percent acidifying food is maintained for optimum health. Further information on interpreting pH levels are published on the GO.pH website at www.goph.recipes.",
        id: "mainText",
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        top: "0",
        width: "90%"
    });
    $.__views.scrollView.add($.__views.mainText);
    exports.destroy = function() {};
    _.extend($, $.__views);
    $.alkalineInfo.titleAttributes = {
        color: "white",
        font: {
            fontFamily: "Roboto",
            fontSize: 14
        }
    };
    $.alkalineInfo.navTintColor = "#ffffff";
    $.alkalineInfo.barColor = Ti.App.Properties.getString("currentColour", "bgcolor");
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;