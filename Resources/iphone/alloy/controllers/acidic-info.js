function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "acidic-info";
    if (arguments[0]) {
        __processArg(arguments[0], "__parentSymbol");
        __processArg(arguments[0], "$model");
        __processArg(arguments[0], "__itemTemplate");
    }
    var $ = this;
    var exports = {};
    $.__views.acidicInfo = Ti.UI.createWindow({
        id: "acidicInfo",
        backgroundColor: "#ffffff",
        barColor: "#ffffff",
        backgroundImage: "images/main_bg.png",
        title: "Acidic",
        titleControl: "#ffffff",
        layout: "vertical"
    });
    $.__views.acidicInfo && $.addTopLevelView($.__views.acidicInfo);
    $.__views.scrollView = Ti.UI.createScrollView({
        id: "scrollView",
        showVerticalScrollIndicator: "true",
        top: "10",
        height: Ti.UI.FILL,
        width: "90%"
    });
    $.__views.acidicInfo.add($.__views.scrollView);
    $.__views.mainText = Ti.UI.createLabel({
        text: "The purpose of an alkaline diet is to bring back the body system from an acidic level of between pH 5 and pH 6.8 to a more normal pH 7 to pH 7.4  You have tested the pH level of your saliva using litmus paper specially designed to indicate its pH value. The result you have entered reflects your pH at that time of the day. It is conditioned by what you have eaten in the past four hours and it reflects your activity level and also your general state of health. One reading however is insufficient to assess your average pH levels. It is recommended that you should record your readings over a period of time say over one week. The GOpH app will enable such records to be maintained.\n\nFor your body to be healthy, nature’s way is to rid the body of irritating acids which depletes bone and tissue of valuable minerals. The body expels these acids in the urine.\n\nThe result you have entered indicates acidification. If you recognise the following symptoms such as acidic saliva - indicated by a reading below PH 7 -  and acidity in the mouth, mouth ulcers and being prone to repeated infections you should consult your health care provider. However, in the meantime by choosing foods which have a high alkalinity as opposed to acidic foods such as red meats will put you on the road to recovery. Health researchers are unanimous in recommending regular meals which have at least 80 percent alkalising foods and 20 per cent acid forming foods. \n\nYour results clearly indicate that at this time acidification is present and to avoid the consequences of your body remaining in an acidic state the recipes in this app will assist in maintaining your body in an alkaline state. Further information on the implications of acidification is available on the GOpH.recipes website which will update as health researchers continue to report on the advantages of maintaining a healthy body and lifestyle through an alkaline forming diet.",
        id: "mainText",
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        top: "0",
        width: "90%"
    });
    $.__views.scrollView.add($.__views.mainText);
    exports.destroy = function() {};
    _.extend($, $.__views);
    $.acidicInfo.titleAttributes = {
        color: "white",
        font: {
            fontFamily: "Roboto",
            fontSize: 14
        }
    };
    $.acidicInfo.navTintColor = "#ffffff";
    $.acidicInfo.barColor = Ti.App.Properties.getString("currentColour", "bgcolor");
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;