function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "meal-listing";
    if (arguments[0]) {
        __processArg(arguments[0], "__parentSymbol");
        __processArg(arguments[0], "$model");
        __processArg(arguments[0], "__itemTemplate");
    }
    var $ = this;
    var exports = {};
    exports.destroy = function() {};
    _.extend($, $.__views);
    $.recipeListing.titleAttributes = {
        color: "white",
        font: {
            fontFamily: "Roboto",
            fontSize: 14
        }
    };
    $.recipeListing.barColor = "#cecece";
    var args = arguments[0] || {};
    $.recipeListing.title = args.data;
    $.recipeListing.navTintColor = "#ffffff";
    var value = Number(Ti.App.Properties.getString("lastReading", "7.0"));
    $.lastRecordedLabel.text = "your last recorded value was: " + value;
    $.lastRecordedLabel.backgroundColor = Ti.App.Properties.getString("currentColour", Alloy.Globals.color_5_6);
    $.levelsTab.addEventListener("click", function(e) {
        Ti.API.info(e.index);
        switch (e.index) {
          case 0:
            $.levelsTab.backgroundColor = "#c9a037";
            break;

          case 1:
            $.levelsTab.backgroundColor = "#225350";
            break;

          case 2:
            $.levelsTab.backgroundColor = "#023867";
        }
    });
    value >= 5 && 6.5 > value ? $.levelsTab.index = 0 : value >= 6.6 && 7.5 > value ? $.levelsTab.index = 1 : value >= 7.6 && 9 >= value && ($.levelsTab.index = 2);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;