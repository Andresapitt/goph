function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "ingredients";
    if (arguments[0]) {
        __processArg(arguments[0], "__parentSymbol");
        __processArg(arguments[0], "$model");
        __processArg(arguments[0], "__itemTemplate");
    }
    var $ = this;
    var exports = {};
    $.__views.ingredients = Ti.UI.createWindow({
        id: "ingredients",
        backgroundColor: "#ffffff",
        barColor: "#5abf93",
        backgroundImage: "images/main_bg.png",
        title: "Ingredients",
        titleControl: "#ffffff",
        layout: "vertical"
    });
    $.__views.ingredients && $.addTopLevelView($.__views.ingredients);
    $.__views.__alloyId6 = Ti.UI.createLabel({
        text: "Ingredients",
        top: "5",
        id: "__alloyId6"
    });
    $.__views.ingredients.add($.__views.__alloyId6);
    $.__views.scrollView = Ti.UI.createScrollView({
        id: "scrollView",
        showVerticalScrollIndicator: "true",
        top: "10",
        height: Ti.UI.FILL,
        width: "90%"
    });
    $.__views.ingredients.add($.__views.scrollView);
    $.__views.mainText = Ti.UI.createLabel({
        color: "#111111",
        font: {
            fontFamily: "Helvetica",
            fontSize: "12dp",
            fontStyle: "normal",
            fontWeight: "normal"
        },
        text: "Aliquam quis platea sagittis magna parturient, egestas ac parturient et? Ac a nisi lectus! Magna habitasse hac et tincidunt! Pellentesque ut nascetur. A platea! Arcu, placerat? Sociis augue lacus pulvinar a augue nunc risus natoque turpis platea sed et dictumst. Scelerisque turpis mid mid? Egestas. Tristique dignissim duis nascetur amet, integer mus sit lectus! Nascetur, ut, in porttitor ac penatibus tincidunt aenean elementum ultricies, odio? Ac ac non massa, natoque, cum dolor? Mid enim odio augue? Nisi in, odio, et, amet, nisi, augue mauris. Et, vut, enim adipiscing nisi nec, magnis ultrices auctor, natoque, non et! Etiam enim integer lorem nisi placerat! Mid magna tortor, magna et ac nisi, cum et, mid et ut, magna! Odio, rhoncus! Eu sed, enim.\n	\n				Tincidunt ut sit, tincidunt purus. Cursus, mid? Aenean enim, vel scelerisque dignissim porttitor dapibus, eros, platea pulvinar hac odio? Porttitor pid sociis elementum magna magnis nunc et porta in. Proin amet? Lacus penatibus cum urna parturient urna lacus auctor penatibus, diam platea mauris? Risus quis placerat phasellus! Penatibus ultricies mus aliquam placerat dignissim, auctor et? Vut a! Aliquet et? Habitasse nec, ultrices duis elit a, pid, urna ut tincidunt in egestas! Augue, a et tortor! Lacus. Aenean! Aenean ac? Adipiscing hac amet odio? Cras quis sit dictumst! Habitasse in? In dictumst nascetur nisi integer cum, augue magnis! Pulvinar phasellus turpis proin, lectus a? Facilisis, quis, eu ut enim dignissim? Porttitor et? Urna magnis, ac! Augue eros etiam elementum phasellus.\n	\n				Mid, risus velit eu et! Amet vut aliquet montes. Cum ultricies? Quis tempor enim? Tristique a amet nascetur ac ut, penatibus placerat eu! Quis placerat! Proin placerat, platea nisi, proin? Magnis a mattis urna a! Nec dapibus, natoque porttitor dis proin proin eros proin, in! Odio magna, etiam est duis et sit magna, habitasse rhoncus adipiscing eu enim? Rhoncus aliquet a porta, rhoncus? Tempor elementum. Rhoncus facilisis porttitor auctor magnis sagittis nisi adipiscing tincidunt! Augue, ac eu elit integer ut etiam quis odio turpis, dapibus rhoncus et urna sociis? Mid platea, elit porttitor scelerisque scelerisque sit nec velit et. Magnis purus, penatibus lectus rhoncus dolor tempor ac vel lundium, habitasse, etiam cum tincidunt. Nisi sociis duis a amet augue.\n	\n				Est sociis integer? Pid augue vel lorem, augue eu amet cursus, placerat elit mid, mus magna, auctor mattis lundium rhoncus cum! Habitasse vel? Est! Dignissim lorem nec tincidunt phasellus? Scelerisque parturient augue et parturient cras! Facilisis egestas sed integer? Urna tincidunt hac enim amet mauris proin platea, odio enim, phasellus dictumst sed massa lectus aenean, facilisis cursus, dapibus urna! Est magna phasellus odio purus! Pid turpis etiam ac lacus, lorem tempor tincidunt ac porta turpis ultrices, phasellus, lectus mus in, nisi magnis porttitor! Sit risus, elementum ac. Integer odio? Placerat habitasse porttitor purus enim velit? Sagittis mid massa augue amet in sociis purus natoque, a est porttitor elit vel amet et etiam integer quis ac tincidunt enim. Amet turpis.",
        id: "mainText",
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        top: "0",
        width: "90%"
    });
    $.__views.scrollView.add($.__views.mainText);
    exports.destroy = function() {};
    _.extend($, $.__views);
    $.ingredients.titleAttributes = {
        color: "white",
        font: {
            fontFamily: "Roboto",
            fontSize: 14
        }
    };
    $.ingredients.navTintColor = "#ffffff";
    var args = arguments[0] || {};
    var db = Ti.Database.open("recipes");
    var recipesRS = db.execute("SELECT id, ingredients FROM recipes WHERE id=" + args.recipeID);
    recipesRS.isValidRow() && ($.mainText.text = recipesRS.fieldByName("ingredients"));
    recipesRS.close();
    db.close();
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;