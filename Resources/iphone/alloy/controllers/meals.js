function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "meals";
    if (arguments[0]) {
        __processArg(arguments[0], "__parentSymbol");
        __processArg(arguments[0], "$model");
        __processArg(arguments[0], "__itemTemplate");
    }
    var $ = this;
    var exports = {};
    $.__views.meals = Ti.UI.createWindow({
        id: "meals",
        barColor: "#5abf93",
        backgroundColor: "#ffffff",
        layout: "vertical",
        title: "Choose a meal"
    });
    $.__views.meals && $.addTopLevelView($.__views.meals);
    $.__views.lastRecordedLabel = Ti.UI.createLabel({
        color: "#ffffff",
        font: {
            fontFamily: "Helvetica",
            fontSize: "15dp",
            fontStyle: "normal",
            fontWeight: "normal"
        },
        text: "your last recorded value was:",
        id: "lastRecordedLabel",
        top: "0",
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        width: "100%",
        height: "30"
    });
    $.__views.meals.add($.__views.lastRecordedLabel);
    $.__views.__alloyId30 = Ti.UI.createView({
        height: Ti.UI.SIZE,
        top: "5",
        id: "__alloyId30"
    });
    $.__views.meals.add($.__views.__alloyId30);
    $.__views.breakfast = Ti.UI.createImageView({
        id: "breakfast",
        image: "/images/",
        top: "5",
        left: "16",
        width: "136",
        height: "163",
        backgroundColor: "#e3e3e3"
    });
    $.__views.__alloyId30.add($.__views.breakfast);
    $.__views.__alloyId31 = Ti.UI.createLabel({
        color: "#111111",
        font: {
            fontFamily: "Helvetica",
            fontSize: "13dp",
            fontStyle: "normal",
            fontWeight: "normal"
        },
        text: "Menu one",
        id: "__alloyId31"
    });
    $.__views.breakfast.add($.__views.__alloyId31);
    $.__views.lunch = Ti.UI.createImageView({
        id: "lunch",
        image: "/images/",
        top: "5",
        right: "16",
        width: "136",
        height: "163",
        backgroundColor: "#e3e3e3"
    });
    $.__views.__alloyId30.add($.__views.lunch);
    $.__views.__alloyId32 = Ti.UI.createLabel({
        color: "#111111",
        font: {
            fontFamily: "Helvetica",
            fontSize: "13dp",
            fontStyle: "normal",
            fontWeight: "normal"
        },
        text: "Menu two",
        id: "__alloyId32"
    });
    $.__views.lunch.add($.__views.__alloyId32);
    $.__views.__alloyId33 = Ti.UI.createView({
        height: Ti.UI.SIZE,
        top: "15",
        id: "__alloyId33"
    });
    $.__views.meals.add($.__views.__alloyId33);
    $.__views.dinner = Ti.UI.createImageView({
        id: "dinner",
        image: "/images/",
        left: "16",
        width: "136",
        height: "163",
        backgroundColor: "#e3e3e3"
    });
    $.__views.__alloyId33.add($.__views.dinner);
    $.__views.__alloyId34 = Ti.UI.createLabel({
        color: "#111111",
        font: {
            fontFamily: "Helvetica",
            fontSize: "13dp",
            fontStyle: "normal",
            fontWeight: "normal"
        },
        text: "Menu Three",
        id: "__alloyId34"
    });
    $.__views.dinner.add($.__views.__alloyId34);
    $.__views.snacks = Ti.UI.createImageView({
        id: "snacks",
        image: "/images/",
        right: "16",
        width: "136",
        height: "163",
        backgroundColor: "#e3e3e3"
    });
    $.__views.__alloyId33.add($.__views.snacks);
    $.__views.__alloyId35 = Ti.UI.createLabel({
        color: "#111111",
        font: {
            fontFamily: "Helvetica",
            fontSize: "13dp",
            fontStyle: "normal",
            fontWeight: "normal"
        },
        text: "Menu four",
        id: "__alloyId35"
    });
    $.__views.snacks.add($.__views.__alloyId35);
    exports.destroy = function() {};
    _.extend($, $.__views);
    $.meals.titleAttributes = {
        color: "white",
        font: {
            fontFamily: "Roboto",
            fontSize: 14
        }
    };
    $.meals.navTintColor = "#ffffff";
    $.lastRecordedLabel.text = "your last recorded value was: " + Ti.App.Properties.getString("lastReading", "7.0");
    $.lastRecordedLabel.backgroundColor = Ti.App.Properties.getString("currentColour", Alloy.Globals.color_5_6);
    $.breakfast.addEventListener("click", function() {
        var args = {
            data: "Menu One"
        };
        var winRecipeListing = Alloy.createController("times-of-day", args).getView();
        Alloy.Globals.navGroup.openWindow(winRecipeListing);
    });
    $.lunch.addEventListener("click", function() {
        var args = {
            data: "Menu Two"
        };
        var winRecipeListing = Alloy.createController("times-of-day", args).getView();
        Alloy.Globals.navGroup.openWindow(winRecipeListing);
    });
    $.dinner.addEventListener("click", function() {
        var args = {
            data: "Menu Three"
        };
        var winRecipeListing = Alloy.createController("times-of-day", args).getView();
        Alloy.Globals.navGroup.openWindow(winRecipeListing);
    });
    $.snacks.addEventListener("click", function() {
        var args = {
            data: "Menu Four"
        };
        var winRecipeListing = Alloy.createController("times-of-day", args).getView();
        Alloy.Globals.navGroup.openWindow(winRecipeListing);
    });
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;