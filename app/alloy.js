// GLOBALS

Alloy.Globals.color_5_0 = "#E0703B";
Alloy.Globals.color_5_2 = "#E1753C";
Alloy.Globals.color_5_4 = "#EA9344";
Alloy.Globals.color_5_6 = "#F0A84D";
Alloy.Globals.color_5_8 = "#F1B250";
Alloy.Globals.color_6_0 = "#E5B74B";
Alloy.Globals.color_6_2 = "#D3BD47";
Alloy.Globals.color_6_4 = "#CCBE45";
Alloy.Globals.color_6_6 = "#9fc63e";
Alloy.Globals.color_6_8 = "#72A963";
Alloy.Globals.color_7_0 = "#7FBB9B";
Alloy.Globals.color_7_2 = "#5BA684";
Alloy.Globals.color_7_4 = "#49979A";
Alloy.Globals.color_7_6 = "#57B1B6";
Alloy.Globals.color_7_8 = "#52ADBC";
Alloy.Globals.color_8_0 = "#4DAABF";
Alloy.Globals.color_8_2 = "#4CA9C0";
Alloy.Globals.color_8_4 = "#449DBF";
Alloy.Globals.color_8_6 = "#3E90BD";
Alloy.Globals.color_8_8 = "#3785BC";
Alloy.Globals.color_9_0 = "#197dbe";

Alloy.Globals.getValueColor = function(value){
	var bgcolor="";
	if (value>=5.0 && value <5.2){
		bgcolor=Alloy.Globals.color_5_0;
		}else if (value>=5.2 && value <5.4){
			bgcolor=Alloy.Globals.color_5_2;
		}else if (value>=5.4 && value <5.6){
			bgcolor=Alloy.Globals.color_5_4;
		}else if (value>=5.6 && value <5.8){
			bgcolor=Alloy.Globals.color_5_6;
		}else if (value>=5.8 && value <6.0){
			bgcolor=Alloy.Globals.color_5_8;
		}else if (value>=6.0 && value <6.2){
			bgcolor=Alloy.Globals.color_6_0;
		}else if (value>=6.2 && value <6.4){
			bgcolor=Alloy.Globals.color_6_2;
		}else if (value>=6.4 && value <6.6){
			bgcolor=Alloy.Globals.color_6_4;
		}else if (value>=6.6 && value <6.8){
			bgcolor=Alloy.Globals.color_6_6;
		}else if (value>=6.8 && value <7.0){
			bgcolor=Alloy.Globals.color_6_8;
		}else if (value>=7.0 && value <7.2){
			bgcolor=Alloy.Globals.color_7_0;
		}else if (value>=7.2 && value <7.4){
			bgcolor=Alloy.Globals.color_7_2;
		}else if (value>=7.4 && value <7.6){
			bgcolor=Alloy.Globals.color_7_4;
		}else if (value>=7.6 && value <7.8){
			bgcolor=Alloy.Globals.color_7_6;
		}  else if (value>=7.8&& value <8.0){
			bgcolor=Alloy.Globals.color_7_8;
		}else if (value>=8.0 && value <8.2){
			bgcolor=Alloy.Globals.color_8_0;
		}else if (value>=8.2 && value <8.4){
			bgcolor=Alloy.Globals.color_8_2;
		}else if (value>=8.4 && value <8.6){
			bgcolor=Alloy.Globals.color_8_4;
		}else if (value>=8.6 && value <8.8){
			bgcolor=Alloy.Globals.color_8_6;
		}else if (value>=8.8 && value <9.0){
			bgcolor=Alloy.Globals.color_8_8;
		}else if (value>=9.0){
			bgcolor=Alloy.Globals.color_9_0;
		}else{
			
		}
		
		return bgcolor;
};


//create localdatabase if it doesnt exist
var dbStats = Ti.Database.open('stats');
dbStats.execute('CREATE TABLE IF NOT EXISTS stats (id INTEGER PRIMARY KEY, year INTEGER, month INTEGER, day INTEGER, week INTEGER, dow TEXT, value NUMERIC);');

//prevent the file identified by this object from being backed up to iTunes.
dbStats.file.setRemoteBackup(false);

dbStats.close();


//create localdatabase if it doesnt exist
var db = Ti.Database.open('recipes');
db.execute('CREATE TABLE IF NOT EXISTS recipes (id INTEGER PRIMARY KEY, wpID INTEGER, title TEXT, date TEXT, modified TEXT, serves TEXT, preparation TEXT, ingredients TEXT, prepTime TEXT, ph_information TEXT, ph_bracket TEXT, ph_value TEXT, mealtime TEXT, backgroundImage TEXT, thumb TEXT, fav TEXT);');

//prevent the file identified by this object from being backed up to iTunes.
db.file.setRemoteBackup(false);


		
// create http client to download images.
var client = Ti.Network.createHTTPClient();
	 
	 
client.onload = function(){
	   var json = JSON.parse(client.responseText);
	   Ti.API.info('json downloaded');
	    processJson(json);
	};
	 
//if any error
client.onerror = function(e){
		Ti.API.info("json error " + JSON.stringify(e));
	    // something went wrong
	    // lets load a internal backup
	    var file = Ti.Filesystem.getFile('data/recipes.json');
	    var data = file.read().text;
	    var json = JSON.parse(data);
	    Ti.API.info("procesing local json");
	    processJson(json);
	};
	 
client.setTimeout(15000);
	 
// get the lastest JSON file  
client.open("GET", "http://goph.recipes/recipes-json/");
	 
//open url
client.send();

Ti.App.addEventListener("resume", function(evt){
	//Redownload json if app was resume to check if there are any updates
	client.send();
});


// process JSON with recipe information and save it to local database.
function processJson(json){
	
	  var i;
	  var recipeID;
      var thumb="";
      var backgroundImage="";
      var mealtime="";
      var serves;
      var prepTime;
      var preparation;
      var ingredients;
      var ph_information;
      var ph_bracket;
      var ph_value;
      var title;
      var date;
      var modified;
      
    
    for (i=0;i<json.length;i++) {
    			var recipe=json[i];
    			recipeID=recipe.id;
	      		thumb=recipe.thumb;
	      		backgroundImage=recipe.bgImage;
	      		mealtime=recipe.mealtime;
	      		serves=recipe.serves;
	      		prepTime=recipe.prepTime;
	    		preparation=recipe.preparation;
	      		
	      		ingredients=recipe.ingredients;
	      		
	      		ph_information=recipe.ph_information;
	      		
	      		ph_bracket=recipe.ph_bracket;
	      		
	      		ph_value=recipe.ph_value;
	      	
	      		title=recipe.title;
	      		
	      		date=recipe.date;
	      		
	      		modified=recipe.modified;
	   
      	var exists= checkIfInDatabase(recipeID,modified);
      	
      	//if id already exist on local database update it otherwise create a new entry
      	if(exists=="update"){
				db.execute('UPDATE recipes SET title="'+title+'", date="'+date+'", modified="'+modified+'", serves="'+serves+'", preparation="'+preparation+'", ingredients="'+ingredients+'", prepTime="'+prepTime+'", ph_information="'+ph_information+'", ph_bracket="'+ph_bracket+'",ph_value="'+ph_value+'", mealtime="'+mealtime+'",backgroundImage="'+backgroundImage+'", thumb="'+thumb+'" WHERE wpID='+recipeID);		
				//save a copy of the image locally
				if(backgroundImage!=false) Alloy.Globals.cachedImageView (recipeID,backgroundImage); // save the background image for recipe
				if(thumb!=false) Alloy.Globals.cachedImageView (recipeID,thumb); // save the thumb image for the recipe
			
      	}else if(exists=="no")
      	{     		
	      		db.execute('INSERT INTO recipes (wpID,title,date,modified,serves,preparation,ingredients,prepTime,ph_information,ph_bracket,ph_value,mealtime,backgroundImage, thumb) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)', recipeID,title, date, modified, serves, preparation,ingredients,prepTime,ph_information,ph_bracket,ph_value,mealtime,backgroundImage,thumb);
				if(backgroundImage!=false) Alloy.Globals.cachedImageView (recipeID,backgroundImage); // save the background image for recipe
				if(thumb!=false) Alloy.Globals.cachedImageView(recipeID,thumb); // save the thumb image for the recipe
      	}//ends if exists
      } // ends for	
db.close();				
} // ends processjson


function checkIfInDatabase(wpID,modified){
	var row = db.execute("SELECT wpID,modified FROM recipes WHERE wpID="+wpID);
		if(row.isValidRow()){
				if(modified>row.fieldByName('modified')){
					
					// already exist check the date and update if local is older
					row.close();	
					return "update";
					
				}else{
					
					//nothing to update
					row.close();
					return "uptodate";
					
				}
				
		}else {   
			
		    //save new entry row to local database 
		    row.close();
		   return "no";
		   
		}
		
}// ends CheckIfInDatabase	





//save images to the local app directory
Alloy.Globals.cachedImageView = function  (imageDirectoryName, url)
{
	// Grab the filename
	var filename = url.split('/');
	filename = filename[filename.length - 1];
	// Try and get the file that has been previously cached
	var file = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, imageDirectoryName, filename);
 
	// If it hasn't been cached, grab the directory it will be stored in.
		var g = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, imageDirectoryName);
		if (!g.exists()) {
			// If the directory doesn't exist, make it
			g.createDirectory();
		};
 
		// Create the HTTP client to download the asset.
		var xhr = Ti.Network.createHTTPClient();
 
		xhr.onload = function() {
			if (xhr.status == 200) {
				// On successful load, take that image file we tried to grab before and 
				// save the remote image data to it.
				file.write(xhr.responseData);
				// file save locally
				return true;
			};
		};
 
		// Issuing a GET request to the remote URL
		xhr.open('GET', url);
		// Finally, sending the request out.
		xhr.send();
};


// iOS version detection
function isiOS4Plus()
{
	// add iphone specific tests
	if (Titanium.Platform.name == 'iPhone OS')
	{
		var version = Titanium.Platform.version.split(".");
		var major = parseInt(version[0],10);
		
		// can only test this support on a 3.2+ device
		if (major >= 4)
		{
			return true;
		}
	}
	return false;
}



//**************************************************************************************
//Notifications
//**************************************************************************************

if(isiOS4Plus()) {
	//the first time the app is run we setup a notification to 11 am
	if(Ti.App.Properties.getBool("notificationFirstTime",true)){
		
		// Check if the device is running iOS 8 or later, before registering for local notifications
		if (Ti.Platform.name == "iPhone OS" && parseInt(Ti.Platform.version.split(".")[0]) >= 8) {
		    Ti.App.iOS.registerUserNotificationSettings({
			    types: [
		            Ti.App.iOS.USER_NOTIFICATION_TYPE_ALERT,
		            Ti.App.iOS.USER_NOTIFICATION_TYPE_BADGE
		        ]
		    });
		}
		
		//set property to avoid setting it up for the second time
		Ti.App.Properties.setBool("notificationFirstTime",false);
		
		 var notificationDate= new Date();
			notificationDate.setHours(Ti.App.Properties.getString("hour","11"));
			notificationDate.setMinutes(Ti.App.Properties.getString("minute","00"));

		 var notification = Ti.App.iOS.scheduleLocalNotification({
		     userInfo: {"id": 1234},
		    alertAction: "GO",
		    alertBody: "Did record your entry today?",
		    date: notificationDate,
		    repeat:"daily",
			}); 
	}	
	
  // listen for a local notification event
  Ti.App.iOS.addEventListener('notification', function(e) {
     Ti.API.info("notification received: " + e.userInfo);
  });

} // ends if isiOS4Plus()


