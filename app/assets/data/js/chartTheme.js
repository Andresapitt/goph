/**
 * Sand-Signika theme for Highcharts JS
 * @author Torstein Honsi
 */

// Load the fonts
Highcharts.createElement('link', {
   href: 'http://fonts.googleapis.com/css?family=Signika:400,700',
   rel: 'stylesheet',
   type: 'text/css'
}, null, document.getElementsByTagName('head')[0]);


Highcharts.theme = {
   colors:['#ffffff', '#ffffff', '#ffffff', '#ffffff', '#ffffff', 
   '#ffffff', '#ffffff', '#ffffff', '#ffffff', '#ffffff'],
   chart: {
      backgroundColor: "#61b792",
      style: {
         fontFamily: "Helvetica,Verdana, san serif"
      }
   },
   title: {
      style: {
         color: 'white',
         fontSize: '11px',
         fontWeight: 'light'
      }
   },
   tooltip: {
      borderWidth: 0
   },
   legend: {
      itemStyle: {
         fontWeight: 'Light',
         fontSize: '10px'
      }
   },
   xAxis: {
   
      labels: {
         style: {
            color: '#ffffff'
         }
      }
   },
   yAxis: {
      labels: {
         style: {
            color: '#ffffff'
         }
      }
   },

   // Highstock specific
   navigator: {
      xAxis: {
         gridLineColor: '#ffffff'
      },style: {
            fontFamily: 'serif',
            fontSize: '10px'
            
        }
   },
   rangeSelector: {
      buttonTheme: {
         fill: 'white',
         stroke: '#ffffff',
         'stroke-width': 2,
         states: {
            select: {
               fill: '#ffffff'
            }
         }
      }
   },
   scrollbar: {
      trackBorderColor: '#ffffff'
   }
   
};

// Apply the theme
Highcharts.setOptions(Highcharts.theme);