/**
 * @author Andrés Pitt
 */
//set title attributes
$.alkalineInfo.titleAttributes=  {
        color:'#0070bb',
        font: {fontFamily:'Roboto', fontSize:16}
};
$.alkalineInfo.titleControl ='#0070bb';
$.alkalineInfo.navTintColor ='#0070bb';
