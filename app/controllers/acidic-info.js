/**
 * @author Andrés Pitt
 */ 
 //set title attributes
$.acidicInfo.titleAttributes=  {
        color:'#ee5503',
        font: {fontFamily:'Roboto', fontSize:16}
};
$.acidicInfo.titleControl ='#ee5503';
$.acidicInfo.navTintColor ='#ee5503';
