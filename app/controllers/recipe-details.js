/**
 * @author Andrés Pitt
 */
//titanium cloud library
var Cloud = require('ti.cloud');

//set title attributes
$.recipeDetails.titleAttributes=  {
        color:Ti.App.Properties.getString("currentColour", "#34beac"),
        font: {fontFamily:'Roboto', fontSize:16}
};
$.recipeDetails.titleControl =Ti.App.Properties.getString("currentColour", "#34beac");
$.recipeDetails.navTintColor =Ti.App.Properties.getString("currentColour", "#34beac");

//parameters from previous window
var args = arguments[0] || {};
var currentColour=Ti.App.Properties.getString("currentColour", "#222222");
$.recipeDetails.barColor =currentColour;

//wordpress id
var wpID;

//check if this window is opened from the stats page
if(args.fromStats == true && args.fromStats!=undefined){
	phValue=args.phValue;
	currentColour=Alloy.Globals.getValueColor(args.value);
	$.recipeDetails.barColor = currentColour;
}

//header and styles for the webviev content
var htmlPrefix="<head><style>body, p, b, strong, em, span, {color:#FFF;font-family:Roboto-light;font-size:1em;} ul,li{color:#fff;font-size:0.9em;} li p{color: #FFF; padding-left: 1em; text-indent: -.7em;font-size:0.9em;} ul > li > span{color:#fff;font-size:0.9em;}  ul {font-size:0.9em;list-style: none; padding:0; margin:0;}li:before {content: '• '; color: "+currentColour+"; font-size:1.5em;}</style></head>";

// favourite button
var rightButton=Ti.UI.createButton({
	image:"images/favorite_false.png",
	width:30,
	height:28,
	fav:false
});

// add favourite button to right navitation
$.recipeDetails.setRightNavButton(rightButton);
rightButton.addEventListener("click",setFavourite);

//bring recipe details from database
var db = Ti.Database.open('recipes');
var recipesRS = db.execute("SELECT id,wpID, ph_information,preparation, ingredients, title, serves,prepTime,ph_bracket, backgroundImage, thumb, fav FROM recipes WHERE id="+args.recipeID);

if (recipesRS.isValidRow())
{
	
	//check if it's a favourite and change icon if true
	if(recipesRS.fieldByName('fav')=="true"){
		rightButton.image="images/favorite_true.png";
		rightButton.fav=true;
	}
	
	
	$.recipeLabel.text=recipesRS.fieldByName('title');
	$.preparationTimeLabel.text=recipesRS.fieldByName('prepTime')+" minutes";
	$.servesLabel.text=recipesRS.fieldByName('serves')+" people";
	$.phInformationLabel.text="pH "+recipesRS.fieldByName('ph_bracket');

	$.phInfoText.html=htmlPrefix+recipesRS.fieldByName('ph_information');
	$.ingredientsText.html=htmlPrefix+recipesRS.fieldByName('ingredients');
	$.preparationText.html=htmlPrefix+recipesRS.fieldByName('preparation');
	wpID=recipesRS.fieldByName('wpID'); //id from wordpress
	var image=recipesRS.fieldByName('thumb');
	var backgroundImage=recipesRS.fieldByName('backgroundImage');
	
	// get filename from url and load the local version of the image
 	var filename = image.split('/');
	 filename = filename[filename.length - 1];
 	var file = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, wpID, filename);
 	var blob = file.read();
 	
 	filename = backgroundImage.split('/');
	 filename = filename[filename.length - 1];
 	file = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, wpID, filename);
 	var bgBlob = file.read();
 
 	// if blob of the image is loaded cropped it and create an image view
 	if(blob!=undefined){
 		Ti.API.info('image didnt exits download a new one');
	var blob2 = blob.imageAsThumbnail(74); //resize keeping aspect ratio
	$.recipeCircle.image=blob2;
	$.recipeDetails.backgroundImage=bgBlob;
	}
	
}	
recipesRS.close(); //close recordset
db.close(); //close database

//add listener to phInfo tab
$.phInfoBtn.addEventListener("click",function(e){
	$.recipeDetailsScrollableView.scrollToView(0); //scroll to first position
	$.phInfoBtnLabel.color=currentColour;
	$.preparationBtnLabel.color="#FFFFFF";
	$.ingredientsBtnLabel.color="#FFFFFF";
	});

//add listener to ingredient tab 
$.ingredientsBtn.addEventListener("click",function(e){
	$.recipeDetailsScrollableView.scrollToView(1); //scroll to second position
	$.phInfoBtnLabel.color="#FFFFFF";
	$.preparationBtnLabel.color="#FFFFFF";
	$.ingredientsBtnLabel.color=currentColour;
	});

//add listener to preparation tab 
$.preparationBtn.addEventListener("click",function(e){
$.recipeDetailsScrollableView.scrollToView(2); //scroll to third position
$.phInfoBtnLabel.color="#FFFFFF";
	$.preparationBtnLabel.color=currentColour;
	$.ingredientsBtnLabel.color="#FFFFFF";
});


//add listener for the the scrolable view is swiped
$.recipeDetailsScrollableView.addEventListener("scrollend", onScroll);
// change the colour of selected tab
function onScroll(e){
	switch(e.currentPage){
		case 0:
		$.phInfoBtnLabel.color=currentColour;
		$.preparationBtnLabel.color="#FFFFFF";
		$.ingredientsBtnLabel.color="#FFFFFF";
		break;
		
		case 1:
		$.phInfoBtnLabel.color="#FFFFFF";
		$.preparationBtnLabel.color="#FFFFFF";
		$.ingredientsBtnLabel.color=currentColour;
		break;
		
		case 2:
		$.phInfoBtnLabel.color="#FFFFFF";
		$.preparationBtnLabel.color=currentColour;
		$.ingredientsBtnLabel.color="#FFFFFF";
		break;
	}
}

// set current recipe as favourite
function setFavourite(e){
	var db = Ti.Database.open('recipes'); //open database to save favourite
	if(rightButton.fav==false){
		db.execute("UPDATE recipes set fav='true' WHERE id="+args.recipeID);
		rightButton.fav=true;
		rightButton.image="images/favorite_true.png";
		$.favourited.animate(a);
	}else{
		db.execute("UPDATE recipes set fav='false' WHERE id="+args.recipeID);
		rightButton.fav=false;
		rightButton.image="images/favorite_false.png";
	}	
	db.close(); // close database
}


// animation for favourites feedback message
var a = Ti.UI.createAnimation({
    opacity : 1,
    duration : 1000,
    autoreverse : true
  });

// adjust positions of iPhone with 3.5" screens
var iPhone5 = (Ti.Platform.displayCaps.platformHeight===568);
if(!iPhone5){
	var newHeight=210;	
	$.detailsCont.height=310;
	$.scrollablePhInfo.height=newHeight;
	$.phInfoText.height=newHeight;
	$.scrollableIngredients.height=newHeight;
	$.ingredientsText.height=newHeight;
	$.scrollablePreparation.height=newHeight;
	$.preparationText.height=newHeight;
}

//send feedback popUp
function sendFeedback(e){
	var myModal = Ti.UI.createWindow({
    title           : 'My Modal',
    backgroundColor : 'transparent'
	});
	
	var wrapperView    = Ti.UI.createView(); // Full screen
	var backgroundView = Ti.UI.createView({  // Also full screen
	    backgroundColor : '#000',
	    opacity         : 0.3
	});
	var containerView  = Ti.UI.createView({  // Set height appropriately
	    height:300,
	    width:280,
	    backgroundImage:"images/popUpBg.png"
	});
	
	var preloaderCont = Ti.UI.createView({  // Set height appropriately
	    height          : 300,
	    backgroundColor : '#000',
	    opacity:0.4,
	    zIndex:3,
	    visible:false 
	});
	
	var preloaderLabel = Ti.UI.createLabel({
		text:"sending...",
		font:{fontSize:15,fontFamily:"Roboto-Light"},
		color:"#fff",
		zIndex:4
	});
	
	preloaderCont.add(preloaderLabel);
	
	var title=Ti.UI.createLabel({
		top:10,
		text:"Send us your comments\nabout this recipe",
		textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
		font:{fontFamily:"Roboto-Light", fontSize:16},
		color:"#ffffff",
		width:220
	});
	
	var textArea = Ti.UI.createTextArea({
	  borderWidth: 1,
	  borderColor: '#404040',
	  borderRadius: 5,
	  color: '#fff',
	  opacity:0.8,
	  backgroundColor:"transparent",
	  font: {fontSize:20, fontFamily:"Roboto-Light"},
	  keyboardType: Titanium.UI.KEYBOARD_EMAIL,
	  top: 60,
	  width: 260, height : 170
	});
  
	var closeButton    = Ti.UI.createButton({
	    title  : 'Close',
	    bottom : 20,
	    right:50,
	    color:"#ffffff"
	});
	
	closeButton.addEventListener('click', function () {
	    myModal.close();
	});
	
	var sendButton    = Ti.UI.createButton({
	    title  : 'Send',
	    bottom : 20,
	    left:50,
	    color:"#ffffff"
	});
	
	//send button listener and sent email via the Titanium Cloud API
	sendButton.addEventListener('click', function () {
		Cloud.Emails.send({
				from:"info@goph.recipes",
				template : 'feedback',
				recipients : 'andresapitt@gmail.com',
				foo : textArea.value
			}, function(e) {
				preloaderCont.visible=true;
				if (e.success) {
					alert('Success');
					preloaderCont.visible=false;
					myModal.close();					
				} else {
					preloaderCont.visible=false;
					alert('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));	
				}
			});
	});
		
	containerView.add(title);
	containerView.add(textArea);
	containerView.add(closeButton);
	containerView.add(sendButton);
	containerView.add(preloaderCont);
	
	wrapperView.add(backgroundView);
	wrapperView.add(containerView);
	
	myModal.add(wrapperView);
	
	myModal.open({
    animate : true
	});
	
	myModal.addEventListener("open", function(event, type) {
   		textArea.blur(); //avoid opening keyboard
	});
}


//send recipe to print
function print(e){
	var myModal = Ti.UI.createWindow({
    title           : 'My Modal',
    backgroundColor : 'transparent'
	});
	
	var wrapperView    = Ti.UI.createView(); // Full screen
	var backgroundView = Ti.UI.createView({  // Also full screen
	    backgroundColor : '#000',
	    opacity         : 0.3
	});
	var containerView  = Ti.UI.createView({  // Set height appropriately
	    height:156,
	    width:280,
	    backgroundImage:"images/popUp-small.png"
	});
	
	var preloaderCont = Ti.UI.createView({  // Set height appropriately
	    height          : 300,
	    backgroundColor : '#000',
	    opacity:0.4,
	    zIndex:3,
	    visible:false 
	});
	
	var preloaderLabel = Ti.UI.createLabel({
		text:"sending...",
		font:{fontSize:15,fontFamily:"Roboto-Light"},
		color:"#fff",
		zIndex:4
	});
	
	preloaderCont.add(preloaderLabel);
	
	var title=Ti.UI.createLabel({
		top:13,
		text:"Enter your email and we will send you this recipe to print",
		textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
		font:{fontFamily:"Roboto-Light", fontSize:16},
		color:"#ffffff",
		width:220
	});
	
	var textArea = Ti.UI.createTextArea({
	  color: '#fff',
	  backgroundColor:"transparent",
	  font: {fontSize:20, fontFamily:"Roboto-Light"},
	  keyboardType: Titanium.UI.KEYBOARD_EMAIL,
	  top: 50,
	  width: 230, height : 40
	});
  
	var closeButton    = Ti.UI.createButton({
	    title  : 'Close',
	    bottom : 20,
	    right:50,
	    color:"#ffffff"
	});
	
	closeButton.addEventListener('click', function () {
	    myModal.close();
	});
	
	var sendButton    = Ti.UI.createButton({
	    title  : 'Send',
	    bottom : 20,
	    left:50,
	    color:"#ffffff"
	});
	
	
	sendButton.addEventListener('click', function(e){	
		Cloud.Emails.send({
				from:"info@goph.recipes",
				template : 'recipe',
				recipients : textArea.value, //send email to the email entered in the textArea
				recipeId : wpID // send as a parameter the recipeID to generate PDF
			}, function(e) {
				preloaderCont.visible=true;
				if (e.success) {
					alert('Please Check your email.');
					preloaderCont.visible=false;
					myModal.close();
				} else {
					preloaderCont.visible=false;
					alert('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));	
				}
			});
	});
			  
	containerView.add(title);
	containerView.add(textArea);
	containerView.add(closeButton);
	containerView.add(sendButton);
	containerView.add(preloaderCont);
	
	wrapperView.add(backgroundView);
	wrapperView.add(containerView);
	
	myModal.add(wrapperView);
	
	
	myModal.open({
    animate : true
	});
	
	myModal.addEventListener("open", function(event, type) {
   		textArea.blur(); //open keyboard by default
 	});
	
} //ends popUp


// first tab color
$.phInfoBtnLabel.color=currentColour;