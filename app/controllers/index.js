/**
 * @author Andrés Pitt
 */
//set title attributes
$.tutorial.titleAttributes=  {
        color:'white',
        font: {fontFamily:'Roboto', fontSize:16}
};
$.tutorial.navTintColor ="#ffffff";


//Attach some simple on/off actions to skip this tutorial
$.checkbox.on = function() {
    this.title='\u2713';
    this.value = true;
};
 
$.checkbox.off = function() {
    this.title='';
    this.value = false;
};
 
//add listener for the switch 
$.basicSwitch.addEventListener('click', function(e) {
    if(false == $.checkbox.value) {
        $.checkbox.on(); // add tickbox
         Ti.App.Properties.setBool("skipTutorial",true); // set persistent property to skip tutorial
    } else {
       $.checkbox.off();
       Ti.App.Properties.setBool("skipTutorial",false); 
    }
});

// when next is pressed move scrollable view to next item
function moveNext(e){
	if($.buttonNext.title=="next"){
		$.tutorialScrolableView.moveNext();
	}else{
		// if it is the last item to to track window.
		var trackWin = Alloy.createController('track').getView();
		trackWin.open();	
	}
}

//change the button title depending on what the current index 
$.tutorialScrolableView.addEventListener("scroll", function(e){
	if($.tutorialScrolableView.getCurrentPage()>2){
		$.buttonNext.title="Ok, I got it";
	}else{
		$.buttonNext.title="next";
	}
	
});

//decide whether skip this window or the track
if(Ti.App.Properties.getBool("skipTutorial",false)){
	var trackWin = Alloy.createController('track').getView();
	trackWin.open();	
}else{
	$.tutorial.open();
}


//adjusting positions for iphone with 3.5" screens
var iPhone5 = (Ti.Platform.displayCaps.platformHeight===568);
if(iPhone5){
$.basicSwitch.top=30;
}

