/**
 * @author Andrés Pitt
 */
var bPages=false; // flag for paging control

//set title attributes
$.recipeListing.titleAttributes=  {
        color:Ti.App.Properties.getString("currentColour", "#34beac"),
        font: {fontFamily:'Roboto', fontSize:16}
};
$.recipeListing.titleControl =Ti.App.Properties.getString("currentColour", "#34beac");
$.recipeListing.navTintColor =Ti.App.Properties.getString("currentColour", "#34beac");

//parameters from times-of-day
var args = arguments[0] || {};

$.recipeListing.title =  args.data; //setting title
//var value = Number(Ti.App.Properties.getString("lastReading","7.0"));

var defaultFontSize = Ti.Platform.name === 'android' ? 16 : 14; // future proofinf for android sizes

var phValue;
var currentColor=Ti.App.Properties.getString("currentColour", "#222222");
var fromStats=args.fromStats;
var value=args.value;

if(args.fromStats == true && args.fromStats!=undefined){
	phValue=args.phValue;
	Ti.API.info('argsvalue: '+args.value);
	currentColor=Alloy.Globals.getValueColor(args.value);
	Ti.API.info('currentColor: '+currentColor);
	$.recipeListing.barColor=currentColor;
}else
{
	phValue=Ti.App.Properties.getString("phValue","Neutral");
}

var db = Ti.Database.open('recipes');


if(args.all==true && args.all!=undefined){
	var recipesRS = db.execute("SELECT id, wpID, title,backgroundImage, thumb, serves,ph_bracket,mealtime,prepTime FROM recipes WHERE mealtime='"+args.mealtime+"'");	
}else{
	var recipesRS = db.execute("SELECT id, wpID, title,backgroundImage, thumb, serves,ph_bracket,mealtime,prepTime FROM recipes WHERE mealtime='"+args.mealtime+"' AND ph_value='"+phValue+"'");
}


var index=0;
var firstImage=true;
var tableData = [];

while (recipesRS.isValidRow())
{
 
  bPages=true;	
  var recipeID = recipesRS.fieldByName('id');
  var wpID = recipesRS.fieldByName('wpID');
  var title = recipesRS.fieldByName('title');
  var serves = recipesRS.fieldByName('serves');
  var phBracket = recipesRS.fieldByName('ph_bracket');
  var cookingTime = recipesRS.fieldByName('prepTime');
  var thumb = recipesRS.fieldByName('thumb');
  var backgroundImage = recipesRS.fieldByName('backgroundImage');
  //Ti.API.info("id: "+recipeID + ' - title: ' + title + ' - serves: ' + serves );
  

// get filename from url and load the local version of the image
 var filename = thumb.split('/');
 filename = filename[filename.length - 1];
 var file = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, wpID, filename);
 var blob = file.read();
 //Ti.API.info("filename: "+filename+"- blob: "+blob);
 
 //backgroundImage
 filename = backgroundImage.split('/');
 filename = filename[filename.length - 1];
 file = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, wpID, filename);
 var bgBlob = file.read();
 
   var myview = Ti.UI.createView({
  		action:"view1",
  		width:270,
  		height:220,
  		top:0,
  		recipeID:recipeID,
  		wpID:wpID,
  		bgImage:blob,
  		viewIndex:index,
  		zIndex:10
  	});
 
 // if blob of the image is loaded cropped it and create an image view
 if(blob!=undefined){
 	// first image
 	if(firstImage){
	$.recipeBackground.backgroundImage=bgBlob;	
 	firstImage=false;
 	}
  	var myImage = Ti.UI.createMaskedImage({
  		action:"view2",
  		mask:"images/recipeListMask.png",
  		image:blob,
  		originalBlob:bgBlob,
  		width:270,
  		height:180,
  		zIndex:11,
  		top:0,
  		touchEnabled:false
  	});
 	myview.add(myImage);
 }else{	
 	Alloy.Globals.cachedImageView(wpID,thumb);
 	Alloy.Globals.cachedImageView(wpID,backgroundImage);
 }
 	

  	var labelCont = Ti.UI.createView({
  		action:"view3",
  		width:Ti.UI.FILL,
  		height:"35",
  		backgroundColor:"#000000",
  		opacity:0.5,
  		touchEnabled:false,
  		bottom:39,
  		zIndex:12
  	});
  	
  	var label = Ti.UI.createLabel({
  		textAlign:Ti.UI.TEXT_ALIGNMENT_LEFT,
  		width: 180,
  		height: 35,
  		left:5,
  		text: title,
  		action:"label",
  		touchEnabled:false,
  		font:{fontFamily:"Roboto", fontSize:"13"},
  		color:"#fff"
  	});
	
	labelCont.add(label);

  	myview.add(labelCont);
 	//rowView.add(view);
 	
 	var phCont = Ti.UI.createView({
  		width:80,
  		height:35,
  		backgroundColor:currentColor,
  		touchEnabled:false,
  		bottom:39,
  		right:0,
  		zIndex:12
  	});
  	
  	var phIcon = Ti.UI.createImageView({
  		image:"images/ph_icon.png",
  		width:12,
  		heigth:15,
  		left:3,
  		touchEnabled:false
  	});
  	
  	var prePhLabel = Ti.UI.createLabel({
  		textAlign:Ti.UI.TEXT_ALIGNMENT_LEFT,
  		width: 15,
  		height: 35,
  		left:19,
  		text: "pH",
  		action:"label",
  		touchEnabled:false,
  		font:{fontFamily:"Roboto Condensed", fontSize:"12"},
  		color:"#fff"
  	});
  	
  	var phLabel = Ti.UI.createLabel({
  		textAlign:Ti.UI.TEXT_ALIGNMENT_LEFT,
  		width: 60,
  		height: 35,
  		left:33,
  		text: phBracket,
  		action:"label",
  		touchEnabled:false,
  		font:{fontFamily:"Roboto Condensed", fontSize:"12"},
  		color:"#fff"
  	});
	
	phCont.add(phIcon);
	phCont.add(prePhLabel);
	phCont.add(phLabel);

  	myview.add(phCont);
  	
  	//cooking time
  	var cookingTimeLabel = Ti.UI.createLabel({
  		textAlign:Ti.UI.TEXT_ALIGNMENT_LEFT,
  		width: 100,
  		height: 35,
  		left:32,
  		bottom:2,
  		text:cookingTime +" minutes",
  		action:"label",
  		touchEnabled:false,
  		font:{fontFamily:"Roboto Light", fontSize:"13"},
  		color:"#818385"
  	});
  	
  	myview.add(cookingTimeLabel);
  	
  	//serves
  	var servesLabel = Ti.UI.createLabel({
  		textAlign:Ti.UI.TEXT_ALIGNMENT_LEFT,
  		width: 100,
  		height: 35,
  		left:162,
  		bottom:2,
  		text:serves+" people",
  		action:"label",
  		touchEnabled:false,
  		font:{fontFamily:"Roboto Light", fontSize:"13"},
  		color:"#818385"
  	});
  	
  	myview.add(servesLabel);
  	
  	//plus icon
  	var plus=Ti.UI.createImageView({
  		width:14,
  		height:14,
  		left:247,
  		top:80,
  		image:"images/arrow.png",
  		zIndex:14,
  		touchEnabled:false
  	});
  	
  	
  myview.add(plus);
  var currentRow=addRow(recipeID,wpID,title,serves,phBracket,cookingTime,blob,thumb);
  tableData.push(currentRow);
  $.recipesList.addView(myview);
  myview.addEventListener("click",openDetails); 
  index++;
  recipesRS.next();
}
recipesRS.close();
db.close();

// open the recipe detilas window    
function openDetails(evt){
	var args = {
        data : $.recipeListing.title, 
        recipeID : evt.source.recipeID,
        wpID: evt.source.wpID,
        fromStats:fromStats,
        value:value
    };
	var winRecipeDetails = Alloy.createController('recipe-details',args).getView();
	Alloy.Globals.navGroup.openWindow(winRecipeDetails);
}

//listener for when the scrolableview was scrolled and update the background image
$.recipesList.addEventListener("scrollend",function(e){
	var viewArray=$.recipesList.getViews();
	$.recipeBackground.backgroundImage=viewArray[e.currentPage].children[0].originalBlob;
});   




//******************************************************
//pagin control
if(bPages){
	

 var pages = [];
 var page;
 var numberOfPages = 0;
 
// pagination for the feature recipes.       
function PagingControl(scrollableView){

     // pagination configuration
    var pageColor=Ti.App.Properties.getString("currentColour", "#34beac");
	var container = Titanium.UI.createView({
		height: 60,
		width:Ti.UI.SIZE,
		top:223
	});
	
	// Keep a global reference of the available pages
	//numberOfPages = $.recipeList.getViews().length;
	var viewArray=$.recipesList.getViews();
	numberOfPages = viewArray.length;
	
	pages = []; // without this, the current page won't work on future references of the module
	
	// Go through each of the current pages available in the scrollableView
	for (var i = 0; i < numberOfPages; i++) {
		page = Titanium.UI.createView({
			borderRadius: 4,
			borderColor:"#fff",
			width: 8,
			height: 8,
			left: 15 * i,
			backgroundColor: pageColor,
			opacity: 0.3
		});
		// Store a reference to this view
		pages.push(page);
		// Add it to the container
		container.add(page);
	}
	
	// Mark the initial selected page
	pages[0].setOpacity(1);
	// Attach the scroll event to this scrollableView, so we know when to update things
	$.recipesList.addEventListener("scroll", onScroll);
        // Reset page control to default page when scollableView refresh
	$.recipesList.addEventListener("postlayout", onPostLayout);
 
	return container;
};
 
onScroll = function(event){
	// Go through each and reset it's opacity
	for (var i = 0; i < numberOfPages; i++) {
		pages[i].setOpacity(0.3);
	}
	// Bump the opacity of the new current page
	pages[event.currentPage].setOpacity(1);
	
};
 
onPostLayout = function(event) {
	// Go through each and reset it's opacity
	for(var i = 0; i < numberOfPages; i++) {
		pages[i].setOpacity(0.3);
	}
	// Bump the opacity of the new current page
	pages[$.recipesList.currentPage].setOpacity(1);
 
};
 
var mypaging= PagingControl($.recipesList);
//mypaging.left=(320/2)-mypaging.width;

$.recipeListing.add(mypaging);
}
		

// Table View
function addRow(recipeID,wpID,title,serves,phBracket,cookingTime,blob){
 
	
  var row = Ti.UI.createTableViewRow({
    className:'recipeRow', // used to improve table performance
    selectedBackgroundColor:'#dadada',
    rowIndex:recipeID, // custom property, useful for determining the row during events
    recipeID:recipeID,
    height:45,
    width:270,
    borderRadius:3
  });
  
  var bottomRowCont = Ti.UI.createView({
  	width:220,
  	height:12,
  	layout:"horizontal",
  	wrapping:false,
  	left:50,
  	bottom:3,
  	touchEnabled:false
  });
  
  var phIcon = Ti.UI.createImageView({
  	image:"images/recipeList-phIcon.png",
  	height:11,
  	width:9,
  	left:0,
  	touchEnabled:false
  });
  
  bottomRowCont.add(phIcon);
  
  var phIconLabel=Ti.UI.createLabel({
  	text:"pH "+phBracket+" |",
  	left:3,
  	width:Ti.UI.SIZE,
  	height:Ti.UI.SIZE,
  	font:{fontFamily:"Roboto-Condensed", fontSize:10},
  	touchEnabled:false
  });
  
  bottomRowCont.add(phIconLabel);
  
  var imageAvatar = Ti.UI.createMaskedImage({
  	mask:"images/circleMask.png",
    image: blob,
    left:7,
    width:38, height:38,
    touchEnabled:false
  });
  row.add(imageAvatar);
  
  var imageBorder= Ti.UI.createImageView({
  	image:"images/recipe-lists-whiteBorder.png",
  	width:41,
  	height:41,
  	left:7,
  	touchEnabled:false
  });
  
  row.add(imageBorder);
  
  var recipeTitle = Ti.UI.createLabel({
    color:'#000000',
    font:{fontFamily:'Roboto-Condensed', fontSize:defaultFontSize,},
    text: title,
    left:54, 
    width:200,
     height:14,
       top:8,
       touchEnabled:false
  });
  row.add(recipeTitle);
  
  var recipeDetails = Ti.UI.createLabel({
    color:'#000000',
    font:{fontFamily:'Roboto-Condensed', fontSize:10},
    text:'serves: ' + serves + ' |',
    left:3,
   	width:Ti.UI.SIZE,
    touchEnabled:false
  });
  
  
  var forks=Ti.UI.createImageView({
  	image:"images/recipe-lists_forks.png",
  	width:6,
  	height:11,
  	left:3,
  	touchEnabled:false
  });
  
  bottomRowCont.add(forks);
  bottomRowCont.add(recipeDetails);
  
  var clock=Ti.UI.createImageView({
  	image:"images/recipe-lists_clock.png",
  	width:11,
  	height:11,
  	left:5,
  	touchEnabled:false
  });
  
  bottomRowCont.add(clock);

 var cookingTime = Ti.UI.createLabel({
    color:'#000000',
    font:{fontFamily:'Roboto-Condensed', fontSize:10},
    text:cookingTime+" minutes",
    left:5,
    touchEnabled:false,
    width:Ti.UI.SIZE,
  });
  bottomRowCont.add(cookingTime);
  
  var arrowIcon= Ti.UI.createImageView({
  	image:"images/recipe-lists-arrow.png",
  	height:14,
  	width:14,
  	right:7,
  	touchEnabled:false
  });
  
  row.add(bottomRowCont);
  
  row.add(arrowIcon);
   
  row.addEventListener("click",openDetails);
  return row;
}

//create bottom table
var tableView = Ti.UI.createTableView({
  backgroundColor:'#fbfbfb',
  borderRadius:5,
  data:tableData,
  height:205,
  width:270,
  bottom:-3
});
$.recipeListing.add(tableView);



//adjusting positions for iphone with 3.5" screens
var iPhone5 = (Ti.Platform.displayCaps.platformHeight===568);
if(!iPhone5){
tableView.height=147;
}


