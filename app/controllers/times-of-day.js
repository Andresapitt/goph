/**
 * @author Andrés Pitt
 */
$.timeOfDay.titleAttributes=  {
        color:Ti.App.Properties.getString("currentColour", "#222222"),
        font: {fontFamily:'Roboto', fontSize:16}
};
$.timeOfDay.titleControl =Ti.App.Properties.getString("currentColour", "#222222");
$.timeOfDay.navTintColor =Ti.App.Properties.getString("currentColour", "#222222");

//parameters from previous window
var args = arguments[0] || {};
var allRecipes=args.all;
var fromStats=args.fromStats;
var phValue=args.phValue;


var value = Number(Ti.App.Properties.getString("lastReading","7.0")); // retrieving what is the latest value entered

if(args.fromStats == true && args.fromStats!=undefined){
	// if we are coming from the stats screen we change the value to what the stats reading was without modifying the current value
	$.timeOfDay.barColor=Alloy.Globals.getValueColor(args.value);
	value=args.value;
}

//breakfast button listener
$.breakfast.addEventListener("click",function(e){
		//parameters to send to the next screen
	var args = {
        data : "Breakfast",
        mealtime: "Breakfast",
        all:allRecipes,
        fromStats:fromStats,
        phValue:phValue,
        value:value       
    };
    var winRecipeListing = Alloy.createController('recipe-listing',args).getView();
	Alloy.Globals.navGroup.openWindow(winRecipeListing);
});

//lunch button listener
$.lunch.addEventListener("click",function(e){
	//parameters to send to the next screen
	var args = {
        data : "Lunch",
        mealtime: "Lunch",
        all:allRecipes,
        fromStats:fromStats,
        phValue:phValue,
        value:value   
    };
    var winRecipeListing = Alloy.createController('recipe-listing',args).getView();
	Alloy.Globals.navGroup.openWindow(winRecipeListing);
});


//dinner button listener
$.dinner.addEventListener("click",function(e){
		//parameters to send to the next screen
	var args = {
        data : "Dinner",
        mealtime: "Dinner",
        all:allRecipes,
        fromStats:fromStats,
        phValue:phValue,
        value:value   
    };
    var winRecipeListing = Alloy.createController('recipe-listing',args).getView();
	Alloy.Globals.navGroup.openWindow(winRecipeListing);
});

//snacks button listener
$.snacks.addEventListener("click",function(e){
		//parameters to send to the next screen
	var args = {
        data : "Snacks",
        mealtime: "Snack",
        all:allRecipes,
        fromStats:fromStats,
        phValue:phValue,
        value:value   
    };
    var winRecipeListing = Alloy.createController('recipe-listing',args).getView();
	Alloy.Globals.navGroup.openWindow(winRecipeListing);
});

