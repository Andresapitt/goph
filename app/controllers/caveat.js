/**
 * @author Andrés Pitt
 */
//set title attributes
$.caveatWindow.titleAttributes=  {
        color:Ti.App.Properties.getString("currentColour", "#34beac"),
        font: {fontFamily:'Roboto', fontSize:16}
};

$.caveatWindow.barColor =Ti.App.Properties.getString("currentColour", "#34beac");
$.caveatWindow.navTintColor =Ti.App.Properties.getString("currentColour", "#34beac");
