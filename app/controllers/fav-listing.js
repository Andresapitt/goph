/**
 * @author Andrés Pitt
 */

var bPages=false;

//set title attributes
$.favListing.titleAttributes={
        color:Ti.App.Properties.getString("currentColour", "#34beac"),
        font:{fontFamily:'Roboto', fontSize:16}
};
$.favListing.barColor =Ti.App.Properties.getString("currentColour", "#34beac");
$.favListing.navTintColor =Ti.App.Properties.getString("currentColour", "#34beac");

//parameters from previous window
var args = arguments[0] || {};

var value = Number(Ti.App.Properties.getString("lastReading","7.0"));


var defaultFontSize = Ti.Platform.name === 'android' ? 16 : 14; // future profing for android devices

function refreshTable(e){
	var db = Ti.Database.open('recipes');
	var recipesRS = db.execute("SELECT id, wpID, title,backgroundImage, thumb, serves,ph_bracket,mealtime,prepTime FROM recipes WHERE fav='true'");

	if(recipesRS.rowCount==0)
	{
		var dialog = Ti.UI.createAlertDialog({
			    title: "You haven't favourited\n any recipe yet",
			    buttonNames: ['Ok']
			 });
			 
		dialog.addEventListener('click', function(e){});
		dialog.show();
	}

	var index=0;
	var firstImage=true;
	var tableData = [];

	while (recipesRS.isValidRow())
	{
	 
	  bPages=true;	
	  var recipeID = recipesRS.fieldByName('id');
	  var wpID = recipesRS.fieldByName('wpID');
	  var title = recipesRS.fieldByName('title');
	  var serves = recipesRS.fieldByName('serves');
	  var phBracket = recipesRS.fieldByName('ph_bracket');
	  var cookingTime = recipesRS.fieldByName('prepTime');
	  var thumb = recipesRS.fieldByName('thumb');
	  var backgroundImage = recipesRS.fieldByName('backgroundImage');
	  Ti.API.info("id: "+recipeID + ' - title: ' + title + ' - serves: ' + serves );
	  
		
	// get filename from url and load the local version of the image
	 var filename = thumb.split('/');
	 filename = filename[filename.length - 1];
	 var file = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, wpID, filename);
	 var blob = file.read();
	 Ti.API.info("filename: "+filename+"- blob: "+blob);
	 
	 //backgroundImage
	 filename = backgroundImage.split('/');
		 filename = filename[filename.length - 1];
	 file = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, wpID, filename);
	 var bgBlob = file.read();
	 
	 
	
	  var currentRow=addRow(recipeID,wpID,title,serves,phBracket,cookingTime,blob,index);
	  tableData.push(currentRow);
	
	  index++;
	  recipesRS.next();
	}
recipesRS.close();
db.close();

tableView.data=tableData;
} //ends refresh table


    
function openDetails(evt){
	Ti.API.info("opening details: "+evt.source.recipeID);
	var args = {
        data : $.favListing.title, 
        recipeID : evt.source.recipeID
    };
	var winRecipeDetails = Alloy.createController('recipe-details',args).getView();
	Alloy.Globals.navGroup.openWindow(winRecipeDetails);
}






	
function phDetails(e){
	
	if(value<=6.9){
 		var phDetailsWindow = Alloy.createController('acidic-info',args).getView();
 	}else if(value>=7.1){
 		var phDetailsWindow = Alloy.createController('alkaline-info',args).getView();
 	}else {
 		var phDetailsWindow = Alloy.createController('neutral-info',args).getView();
 	}	
 	
 	Alloy.Globals.navGroup.openWindow(phDetailsWindow);
}



       


// Table View



function addRow(recipeID,wpID,title,serves,phBracket,cookingTime,blob,index){
 
  var row = Ti.UI.createTableViewRow({
    className:'recipeRow', // used to improve table performance
    selectedBackgroundColor:'#F7F7F7',
    rowIndex:recipeID, // custom property, useful for determining the row during events
    recipeID:recipeID,
    height:55,
    width:320,
    borderRadius:3,
    opacity:0
  });
  
  var a = Ti.UI.createAnimation({
    opacity : 1,
    duration : 500*index
  });
  
  var imageAvatar = Ti.UI.createMaskedImage({
  	mask:"images/circleMask.png",
    image: blob,
    left:10,
    width:38, height:38,
    touchEnabled:false
  });
  row.add(imageAvatar);
  
  var imageBorder= Ti.UI.createImageView({
  	image:"images/recipe-lists-whiteBorder.png",
  	width:41,
  	height:41,
  	left:10,
  	touchEnabled:false
  	
  });
  
  row.add(imageBorder);
  
  var recipeTitle = Ti.UI.createLabel({
    color:'#000000',
    font:{fontFamily:'Roboto-Light', fontSize:defaultFontSize,},
    text: title,
    left:56, 
    width:260,
    height:14,
    top:8,
    touchEnabled:false
  });
  row.add(recipeTitle);
  
  var recipeDetails = Ti.UI.createLabel({
    color:'#000000',
    font:{fontFamily:'Roboto-Light', fontSize:10},
    text:'serves: ' + serves + '  |         '+cookingTime+" minutes  |",
    left:68, bottom:5,
    width:150,
    height:20,
    touchEnabled:false
  });
  row.add(recipeDetails);
  
  var forks=Ti.UI.createImageView({
  	image:"images/recipe-lists_forks.png",
  	width:6,
  	height:11,
  	bottom:10,
  	left:57,
  	touchEnabled:false
  });
  
  row.add(forks);
  
  var clock=Ti.UI.createImageView({
  	image:"images/recipe-lists_clock.png",
  	width:11,
  	height:11,
  	bottom:10,
  	left:119,
  	touchEnabled:false
  });
  
  row.add(clock);
  
   var phIcon = Ti.UI.createImageView({
  	image:"images/recipeList-phIcon.png",
  	height:11,
  	width:9,
  	left:185,
  	bottom:10,
  	touchEnabled:false
  });
  
  row.add(phIcon);
  
    var phIconLabel=Ti.UI.createLabel({
  	text:"pH:",
  	left:197,
  	bottom:9,
  	width:Ti.UI.SIZE,
  	height:Ti.UI.SIZE,
  	font:{fontFamily:"Roboto-Light", fontSize:10},
  	touchEnabled:false
  });
  
  row.add(phIconLabel);
  
  var phBracketLabel= Ti.UI.createLabel({
  	text:phBracket,
  	left:215,
  	bottom:8,
  	width:Ti.UI.SIZE,
  	height:Ti.UI.SIZE,
  	font:{fontFamily:"Roboto-Light", fontSize:10},
  	touchEnabled:false
  });
  
  row.add(phBracketLabel);

  
  var arrowIcon= Ti.UI.createImageView({
  	image:"images/recipe-lists-arrow.png",
  	height:14,
  	width:14,
  	right:7,
  	touchEnabled:false
  });
  
  row.add(arrowIcon);
  row.animate(a); 
  row.addEventListener("click",openDetails);
  return row;
}

var tableView = Ti.UI.createTableView({
  backgroundColor:'white',
  height:Ti.UI.FILL,
  width:Ti.UI.FILL,
  top:0
});
$.favListing.add(tableView);

var iPhone5 = (Ti.Platform.displayCaps.platformHeight===568);

if(!iPhone5){
tableView.height=147;
}

$.favListing.addEventListener("focus",refreshTable);

//refreshTable();
