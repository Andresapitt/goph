/**
 * @author Andrés Pitt
 */

//set title attributes
$.neutralInfo.titleAttributes=  {
        color:'#87ba33',
        font: {fontFamily:'Roboto', fontSize:16}
};

$.neutralInfo.titleControl ='#87ba33';
$.neutralInfo.navTintColor ='#87ba33';

