/**
 * @author Andrés Pitt
 */
//set title attributes
$.recipeListing.titleAttributes=  {
        color:Ti.App.Properties.getString("currentColour", "#34beac"),
        font: {fontFamily:'Roboto', fontSize:16}
};

$.recipeListing.barColor =Ti.App.Properties.getString("currentColour", "#34beac");
$.recipeListing.navTintColor =Ti.App.Properties.getString("currentColour", "#34beac");

//parameters from times-of-day
var args = arguments[0] || {};

$.recipeListing.title =  args.data; //setting title
$.recipeListing.navTintColor ="#ffffff";
var value = Number(Ti.App.Properties.getString("lastReading","7.0"));
$.lastRecordedLabel.text="your last recorded value was: "+value; //last saved reading
$.lastRecordedLabel.backgroundColor=Ti.App.Properties.getString("currentColour", Alloy.Globals.color_5_6); //changing label background color

//
$.levelsTab.addEventListener("click",function(e){
	Ti.API.info(e.index);
	switch(e.index){
		case 0:
		$.levelsTab.backgroundColor="#c9a037";
		//$.removeClass($.View, "recipeButtonLow recipeButtonMedium recipeButtonHigh");
		break;
		
		case 1:
		$.levelsTab.backgroundColor="#225350";
		//$.removeClass($.View, "recipeButtonLow recipeButtonMedium recipeButtonHigh");
		break;
		
		case 2:
		$.levelsTab.backgroundColor="#023867";
		//$.removeClass($.View, "recipeButtonLow recipeButtonMedium recipeButtonHigh");
		break;
	}
});


//change tab to last reading bracket 
if (value>=5 && value <6.5){
		$.levelsTab.index=0;		
}
else if(value>=6.6 && value <7.5){
	$.levelsTab.index=1;
}else if(value>=7.6 && value <=9){
	$.levelsTab.index=2;	
}
