/**
 * @author Andrés Pitt
 */
//set title attributes
$.moreInfo.titleAttributes=  {
        color:Ti.App.Properties.getString("currentColour", "#34beac"),
        font: {fontFamily:'Roboto', fontSize:16}
};

$.moreInfo.titleControl =Ti.App.Properties.getString("currentColour", "#34beac");
$.moreInfo.navTintColor =Ti.App.Properties.getString("currentColour", "#34beac");

//add listener to open Caveat window 
Ti.App.addEventListener("caveat",openCaveat);
 
 // open Caveat window
 function openCaveat(e){
 	var caveatWin = Alloy.createController('caveat').getView();
 	Alloy.Globals.navGroup.openWindow(caveatWin);
 }