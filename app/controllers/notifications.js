/**
 * @author Andrés Pitt
 */

//set title attributes
$.winNotifications.titleAttributes=  {
        color:Ti.App.Properties.getString("currentColour", "#34beac"),
        font: {fontFamily:'Roboto', fontSize:16}
};
$.winNotifications.titleControl =Ti.App.Properties.getString("currentColour", "#34beac");
$.winNotifications.navTintColor =Ti.App.Properties.getString("currentColour", "#34beac");

//create picker to select the time for notifications
var picker = Ti.UI.createPicker({
	  top:85,
	  type:Ti.UI.PICKER_TYPE_TIME,
	  useSpinner: true
});

var hour;
var minute;
var d=new Date();	

// listener for when the picker changes.
picker.addEventListener('change',function(e) {
  d = new Date(e.value); //create a new date object with the selected time
  hour = d.getHours();
  minute = d.getMinutes();
  setNotificationTime(); //set notification to the selected time
});


//change notifications on or off
function notificationSwitch(e){
	if(!$.basicSwitch.value){
		picker.visible=false;
	 	$.confirmation.visible=false;
	 	$.notificationsLabel.visible=false;
	 	$.bottomLine.visible=false;
	 	$.alarmImage.visible=false;
	 	Ti.App.iOS.cancelLocalNotification(1234); //cancel notification
	 	Titanium.App.iOS.cancelAllLocalNotifications(); //cancel notification in older devices
	}
	else{
		picker.visible=true;
	 	$.confirmation.visible=true;
		$.notificationsLabel.visible=true;
		$.bottomLine.visible=true;
		$.alarmImage.visible=true;
		setNotificationTime(); // set notification to the last selected time
		$.confirmation.text="Your will be reminded everyday at: "+formatTime(Ti.App.Properties.getString("hour",hour)) +":"+formatTime(Ti.App.Properties.getString("minute",minute));
	}
}



function setNotificationTime (){
	//set hour and minute
	Ti.App.Properties.setString("hour",hour);
	Ti.App.Properties.setString("minute",minute);
	//eschudel notification
	$.confirmation.text="Your will be reminded everyday at: "+formatTime(hour) +":"+formatTime(minute);
	Ti.App.iOS.cancelLocalNotification(1234); //cancel notification
	Titanium.App.iOS.cancelAllLocalNotifications(); //cancel notification in older devices

	notificationDate.setHours(Ti.App.Properties.getString("hour","11")); //set the time to 11 am if a the property hour is not set
	notificationDate.setMinutes(Ti.App.Properties.getString("minute","00"));
	
	// The following code snippet schedules an alert to be sent within three seconds
	var notification = Ti.App.iOS.scheduleLocalNotification({
     userInfo: {"id": 1234},
    alertAction: "GO",
    // Alert will display the following message
    alertBody: "Did record your entry today?",
    date: notificationDate,
    repeat:"daily",
	}); 


}

// append 0 to numbers under 10
function formatTime(value){
	if(value<10)
	return "0"+value;
	else
	return value;
}	
	
//create a date object for the notification	
var notificationDate= new Date();
notificationDate.setHours(Ti.App.Properties.getString("hour","11"));
notificationDate.setMinutes(Ti.App.Properties.getString("minute","00"));

picker.value=notificationDate; // sets the picker on the last selected time first time this window is opened
$.confirmation.text="Your will be reminded everyday at: "+formatTime(Ti.App.Properties.getString("hour",hour)) +":"+formatTime(Ti.App.Properties.getString("minute",minute));



//add picker to window
$.winNotifications.add(picker);

