/**
 * @author Andrés Pitt
 */
var moment = require('moment'); // library for calculating dates

// title and navigation attributes
$.stats.titleAttributes=  {
        color:Ti.App.Properties.getString("currentColour", "#34beac"),
        font: {fontFamily:'Roboto', fontSize:16}
};
$.stats.titleControl =Ti.App.Properties.getString("currentColour", "#34beac");
$.stats.navTintColor =Ti.App.Properties.getString("currentColour", "#34beac");


var thisWeek=moment().format("w");
var thisYear=moment().format("YYYY");
var thisMonth=moment().format("M");
thisMonth=thisMonth-1;
var thisDay=moment().format("D");
var previousWeek= moment().subtract(1, 'w').format("w");
updateDateLabel(thisWeek);

alert("This section has been pre populated with dummy entries to facilitate testing");

//parameters from previuos window
var args = arguments[0] || {};

var aThisWeek = new Array();
var aThisWeekDate = new Array();

//create 3d matrix to roate arrow icon
var t = Ti.UI.create2DMatrix();
t= t.rotate(90);
$.weekArrow.transform=t;

// update stats display
function updateStats(week,year){
	thisWeek=week;
	thisYear=year;
	previousWeek= week-1;
	if(previousWeek<1) previousWeek = 54;
	getData();
}


//query database to get entries per selected week and passes the entries to the chart
function getData(e){
	
	aThisWeek=[];
	aThisWeekDate=[];
	var index=0;
	
	for(index=0;index<=6;index++){ 	
		 aThisWeek[index]=null;
		 aThisWeekDate[index]=null;	
	}
	
	var year=thisYear; // in case there is no readings
	thisYear=year;
	var month= thisMonth;
	thisMonth=month;
	var day= thisDay;
	var dayOfTheWeek=moment(year+"-"+month+"-"+day, "YYYY-MM-DD").format("d");
	var date=moment(year+"-"+month+"-"+day, "YYYY-MM-DD").format("Do");
	var dayName=moment(year+"-"+month+"-"+day, "YYYY-MM-DD").format("ddd");
	thisDay=moment(year+"-"+month+"-"+day, "YYYY-MM-DD").startOf('week');

	var db = Ti.Database.open('stats');
	
	var statsRS = db.execute("SELECT id, year, month , day, dow, value FROM stats WHERE week="+thisWeek+" AND year="+thisYear);
	
	while(statsRS.isValidRow()) {		
		 var value = statsRS.fieldByName('value');
		 year=statsRS.fieldByName('year');
		 thisYear=year;
		 month= statsRS.fieldByName('month');
		 thisMonth=month;
		 day= statsRS.fieldByName('day');
		 dayOfTheWeek=moment(year+"-"+month+"-"+day, "YYYY-MM-DD").format("d");
		 date=moment(year+"-"+month+"-"+day, "YYYY-MM-DD").format("Do");
		 dayName=moment(year+"-"+month+"-"+day, "YYYY-MM-DD").format("ddd");
		thisDay=moment(year+"-"+month+"-"+day, "YYYY-MM-DD").startOf('week');
		aThisWeek[dayOfTheWeek]=value;
		statsRS.next();
	}
	statsRS.close();
	db.close();
	
	
	for(index=0;index<=6;index++){ 	
		var day=moment(thisDay).day(index).format("ddd");
		var date= moment(thisDay).day(index).format("Do");
		aThisWeekDate[index]=day+" "+date;
	}
	
	Ti.App.fireEvent("dataCallback",{aThisWeek:aThisWeek,aThisWeekDate:aThisWeekDate,year:thisYear , month: thisMonth, day: thisDay}); // send entries to the chart
} // ends get data

getData(); // gets the data to the chart the first time this window is opened

//lustner for when the wbeview is opened
$.statsWebview.addEventListener("load", function(e){	
	Ti.App.fireEvent("dataCallback",{aThisWeek:aThisWeek,aThisWeekDate:aThisWeekDate });
	$.statsWebview.visible=true;
});

// listener for when a dot is tapped
Ti.App.addEventListener("TOOLTIP",function(e){
  $.selectedEntry.text='Your pH reading on '+ e.day +' was: ' + e.value;
  $.selectedEntryCont.value=e.value;
  $.selectedEntryCont.animate(fadeInSelectedReading);
  myCounter.start(); //start timer to fadeout the reading
});


var iPhone5 = (Ti.Platform.displayCaps.platformHeight===568);
if(iPhone5){
//$.lastReadingContainer.visible=true;
//$.phInfo.bottom=10;
}

// listener to open a recipe to a past value
$.selectedEntryCont.addEventListener("click",openRecipe);

//open recipe for that specific day
function openRecipe(e){
	
	var value=e.source.value;
	var phValue="";
	
	if(value<6.8){
 		phValue="Acidic";
 	}else if(value>=6.8 && value<=7.2){
 		phValue="Neutral";
 	}else {
 		phValue="Alkaline";
 	}
 	
 	// parameters for next window
 	var args = {
        fromStats : true,
         phValue: phValue,
        value:value
    };
    
 	var mealTimes = Alloy.createController('times-of-day',args).getView();
	Alloy.Globals.navGroup.openWindow(mealTimes);
}

// percentage of alkaline, neutral and acidic readings
$.percentageWebview.addEventListener("load", function(e){
	//total number of entries
	var db = Ti.Database.open('stats');
	var row = db.execute('SELECT count(id) as HowMany from stats');    
	$.totalNumber.text="Entries since you installed this app: "+row.fieldByName('HowMany');
	totalEntries=row.fieldByName('HowMany');
	row.close();
	
	//total number of acidic entries
	var row = db.execute('SELECT count(id) as HowMany from stats WHERE value<6.8');    
	var totalAcidic=row.fieldByName('HowMany');
	row.close();
	var acidicPercentage=Math.floor((totalAcidic*100)/totalEntries);
	$.totalAcidic.text=acidicPercentage+"%";
	
	
	//total number of acidic neutral
	var row = db.execute('SELECT count(id) as HowMany from stats WHERE value>=6.8 and value<7.2');    
	var totalNeutral=row.fieldByName('HowMany');
	row.close();
	var neutralPercentage=Math.floor((totalNeutral*100)/totalEntries);
	$.totalNeutral.text=neutralPercentage+"%";
	
	//total number of acidic neutral
	var row = db.execute('SELECT count(id) as HowMany from stats WHERE value>=7.2');    
	var totalAlkaline=row.fieldByName('HowMany');
	row.close();
	db.close();	
	var alkalinePercentage=Math.floor((totalAlkaline*100)/totalEntries);
	$.totalAlkaline.text=alkalinePercentage+"%";
	//now that I have all the value pass them on to a webview to draw them on a pie chart
	Ti.App.fireEvent("updateStats",{alkaline:alkalinePercentage,acidic:acidicPercentage,neutral:neutralPercentage});
});


//Week picker to select previous weeks
var picker_view = Titanium.UI.createView({
	visible:true,
	zIndex:10,
	height:Ti.UI.SIZE,
	bottom:-300,
	closed:true
	});
	 
	var cancel =  Titanium.UI.createButton({
		title:'Cancel',
		style:Titanium.UI.iPhone.SystemButtonStyle.BORDERED
	});
	
	cancel.addEventListener("click", function(e){
		animateArrow(a);
		picker_view.animate(hidePickerAnimation);
		picker_view.closed=true;
	});
	 
	var done =  Titanium.UI.createButton({
		title:'Done',
		style:Titanium.UI.iPhone.SystemButtonStyle.DONE
	});
	
	done.addEventListener("click", function(e){
		picker_view.closed=true;
		animateArrow(a);
		picker_view.animate(hidePickerAnimation);
	});
	 
	 
	var spacer =  Titanium.UI.createButton({
		systemButton:Titanium.UI.iPhone.SystemButton.FLEXIBLE_SPACE
	});
	 
	var toolbar =   Ti.UI.iOS.createToolbar({
		top:0,
		items:[cancel,spacer,done]
	});
	 
	var picker = Ti.UI.createPicker({
	  top:43,
	  useSpinner: true
	});
	
	// check database for entries grouped them by week.
	var db = Ti.Database.open('stats');
	var row = db.execute('SELECT week from stats GROUP BY week ORDER BY week DESC'); 
	var index=0;
	var data = [];
	while(row.isValidRow()){
		var week=row.fieldByName('week');
		var endOfWeek=moment(week,"w");
		var year=endOfWeek.year();
		endOfWeek=endOfWeek.startOf("week").format("dddd, Do MMMM YYYY"); //day, date month
		
		data[index]=Ti.UI.createPickerRow({title:endOfWeek, week:week, year:year});
		index++;
		row.next();	
	}
	row.close();
	db.close();
	
	
	picker.add(data);
	picker.selectionIndicator = true;
	 
	 
	picker_view.add(toolbar);
	picker_view.add(picker);
	
	$.stats.add( picker_view );
	
	var matrix2d = Ti.UI.create2DMatrix();
	matrix2dA = matrix2d.rotate(90); // in degrees
	matrix2dB = matrix2d.rotate(0); // in degrees
	
	
	var a = Ti.UI.createAnimation({
	    transform: matrix2dA,
	    duration: 500,
	    autoreverse: false
	});
	
	var b = Ti.UI.createAnimation({
	    transform: matrix2dB,
	    duration: 500,
	    autoreverse: false
	});
	
	var showPickerAnimation = Ti.UI.createAnimation({
	    bottom:0,
	    duration: 500,
	    autoreverse: false
	});
	
	var hidePickerAnimation = Ti.UI.createAnimation({
	    bottom:-300,
	    duration: 500,
	    autoreverse: false
	});
	
 
	function chooseWeek(e){
		
		if(picker_view.closed==false){
			picker_view.closed=true;
			animateArrow(a);
			picker_view.animate(hidePickerAnimation);
		}else{
			picker_view.closed=false;
			animateArrow(b);
			picker_view.animate(showPickerAnimation);
		}
		
	}
	
	
	function animateArrow(animation){
		$.weekArrow.animate(animation);
	}
	
	
	function onPickerChange(e){
		var week=moment(picker.getSelectedRow(0).week,"w");

		updateDateLabel(week);
		
		updateStats(picker.getSelectedRow(0).week,picker.getSelectedRow(0).year);
	}
	
	picker.addEventListener('change',onPickerChange);
	
	function updateDateLabel(week){
		var week=moment(week,"w");
		var month=week.format("MMM");
		var year=week.format("YYYY");
		var startOfWeek=week.startOf("week").format("Do"); 
		var endOfWeek=week.endOf("week").format("Do");
		
		$.weekLabel.text=startOfWeek+" - "+endOfWeek;
		//$.monthLabel.text=month+" "+year;
		$.monthLabel.text=month;
	}

var fadeInSelectedReading = Ti.UI.createAnimation({
	    opacity: 1,
	    duration: 800,
	    autoreverse: false
	});
	
var fadeOutSelectedReading = Ti.UI.createAnimation({
	    opacity: 0,
	    duration: 800,
	    autoreverse: false
	});

fadeInSelectedReading.addEventListener("complete",function(e){

});

function fadeSelectedReading(evt){
	$.selectedEntryCont.animate(fadeOutSelectedReading);
}


// counter to fadeOut the SelectedReading.
function Countdown(options) {
  var timer,
  instance = this,
  seconds = options.seconds || 4,
  updateStatus = options.onUpdateStatus || function () {},
  counterEnd = options.onCounterEnd || function () {};

  function decrementCounter() {
    updateStatus(seconds);
    if (seconds === 0) {
      counterEnd();
      instance.stop();
    }
    seconds--;
  }

  this.start = function () {
    clearInterval(timer);
    timer = 0;
    seconds = options.seconds;
    timer = setInterval(decrementCounter, 1000);
  };

  this.stop = function () {
    clearInterval(timer);
  };
}

// counter for whena dot is tapped to show information for 4 seconds and then fade it out
var myCounter = new Countdown({  
    seconds:4,  // number of seconds to count down
    onUpdateStatus: function(sec){Ti.API.info('counter:'+sec);}, // callback for each second
    onCounterEnd: function(){ fadeSelectedReading(); } //fade out the selected reading
});


// Adjustments for 3.5" iPhones
var iPhone5 = (Ti.Platform.displayCaps.platformHeight===568);
if(!iPhone5){
$.percentagesLabels.top=43;	
$.percentageWebviewLabels.top=95;
$.percentageWebview.top=25;
}
