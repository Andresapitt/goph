Alloy.Globals.navGroup = $.winMain; // sets navigation group

// sets title attributes
$.win1.titleAttributes=  {
        color:Ti.App.Properties.getString("currentColour", "#34beac"),
        font: {fontFamily:'Roboto-Bold', fontSize:16}
};

$.win1.titleControl =Ti.App.Properties.getString("currentColour", "#34beac");
$.win1.navTintColor =Ti.App.Properties.getString("currentColour", "#34beac");

var moment = require('moment'); //include library fro date management

var numb; // variable to store reading from the dial

numb = Ti.App.Properties.getString("lastReading","7.0"); // reads the last reading or sets it to 7.0 if there isn't one

/****************************************
Dial
****************************************/
//set up matrixes to transform the dial
	var t1 = Ti.UI.create2DMatrix();
	var t2 = Ti.UI.create2DMatrix();
	var t3 = Ti.UI.create2DMatrix();
	var t4 = Ti.UI.create2DMatrix();
	
	var percent;
	var angleArc=135;
	var angleOffset=249;
	t1 = t1.rotate(angleOffset); //angleOffset


//create a tranparent needle to "host" the thumb/knob around the dial
	var bar=Ti.UI.createView({
		 backgroundColor:"transparent",
		  width:30,
		  height:165,
		  anchorPoint: {x:0.5,y:1},
		   transform:t1,
		   top:-15,
		   left:125,
		   touchEnabled:false,
		   zIndex:10
	});
	
	var knob=Ti.UI.createImageView({
		image:"images/slider-knob.png",
		height:40,
		width:50,
		top: 10,
		touchEnabled:false,
		zIndex:4
	});
	
	var barLabel=Ti.UI.createLabel({
		text:"7.0",
		width:Ti.UI.SIZE,
		height:Ti.UI.SIZE,
		top:-10,
		font:{fontSize:14},
		touchEnabled:false,
		zIndex:5
	});
	
	bar.add(barLabel);
	bar.add(knob);

	$.dialContainer.add(bar);
	t2 = t2.rotate(90);
	$.dialContainer.transform=t2;
	t3 = t3.rotate(-90);
	$.sliderGradient.transform=t3;
	t4= t4.rotate(45);
	$.arrowPointer.transform=t4;
	

/****************************************
Last two readings
****************************************/
// arrow for the last two readings
var arrow1 = Titanium.UI.createMaskedImage({
    mask : '/images/arrow.png', // background image
    tint: Ti.App.Properties.getString("currentColour", "#5abf93"),
    right:7,
    width:14, 
    height:14,
    touchEnabled:false
});

var bullet1 = Titanium.UI.createMaskedImage({
    mask : '/images/bullet.png', // background image
    tint: Ti.App.Properties.getString("currentColour", "#5abf93"),
    left:15,
    width:7, 
    height:7,
    touchEnabled:false
});

var editCont= Ti.UI.createView({
	width:45,
	height:27,
	left:0,
	backgroundColor:"transparent"
});

var edit = Ti.UI.createMaskedImage({
	mask:"/images/edit.png",
	tint:Ti.App.Properties.getString("currentColour", "#5abf93"),
	width:12,
	height:12,
	left:-15,
	touchEnabled:false
});

$.lastReading1.add(arrow1);
$.lastReading1.add(bullet1);
$.lastReading1.add(editCont);
$.lastReading1.add(edit);


var arrow2 = Titanium.UI.createMaskedImage({
    mask : '/images/arrow.png', // background image
    tint: Ti.App.Properties.getString("currentColour", "#5abf93"),
    right:7,
    width:14, 
    height:14,
    touchEnabled:false
});

var bullet2 = Titanium.UI.createMaskedImage({
    mask : '/images/bullet.png', // background image
    tint: Ti.App.Properties.getString("currentColour", "#5abf93"),
    left:15,
    width:7, 
    height:7,
    touchEnabled:false
});

$.lastReading2.add(arrow2);
$.lastReading2.add(bullet2);

//show the last two reading	
function updateLastReading(){
	var db = Ti.Database.open('stats');
	var statsRS = db.execute("SELECT id, year, month , day, dow, week, value FROM stats ORDER BY id DESC LIMIT 3");
	 	if(statsRS.isValidRow()) {
	 		var month=moment(statsRS.fieldByName("month"), "MM").format("MMM");
	 		var day = moment(statsRS.fieldByName("day"), "DD").format("Do");
	 		$.lastReadingDate.text=statsRS.fieldByName("dow")+" "+day+", "+month+" "+statsRS.fieldByName("year")+" | Week "+statsRS.fieldByName("week");
	 		var value = statsRS.fieldByName("value");
	 		$.lastReadingLabel.text=value.toFixed(1);
	 		updateBulletColor(statsRS.fieldByName("value"),bullet1,arrow1);
	 		
	 		//move to next entry on the db
	 		statsRS.next();
	 		var month=moment(statsRS.fieldByName("month"), "MM").format("MMM");
	 		var day = moment(statsRS.fieldByName("day"), "DD").format("Do");
	 		var value = statsRS.fieldByName("value");
	 		$.lastReadingLabelb.text=value.toFixed(1);
	 		$.lastReadingDateb.text=statsRS.fieldByName("dow")+" "+day+", "+month+" "+statsRS.fieldByName("year")+" | Week "+statsRS.fieldByName("week");
	 		updateBulletColor(statsRS.fieldByName("value"),bullet2,arrow2);
	 	}
	db.close();
	statsRS.close();
	$.currentDate.text= moment().format("MMM") + " " + moment().format("Do");
}


// create a db with dummy entries to be able to test the Stats/progress screen
if(Ti.App.Properties.getBool("dummyEntries",true)){	
	var db = Ti.Database.open('stats');
	for(var i=134;i>1;i--){
		    var mo= moment().subtract(i, 'd');
		    var month= mo.format("M");
	   		Ti.API.info("month: " + month);
	   		var day=mo.format("DD");
			Ti.API.info("day of month: "+ day);
			var dow=mo.format("ddd");
			Ti.API.info("day of week: "+dow);
			var week=mo.format("w");
			Ti.API.info("week of year: " + week);
			var year = mo.format("YYYY");
			var value= Math.random()*(8-6+1)+6;
			value=value.toFixed(1);
			Ti.API.info('date: ' + day + '/' + month + '/'+year+" - week: "+week+" - value: "+value );
			db.execute("INSERT INTO stats (value,day,month,year,week, dow) VALUES (?,?,?,?,?,?)",value, day,month,year,week,dow);
	}
	db.close();
	Ti.App.Properties.setBool("dummyEntries",false); //to avoid setting new entries the next time is open?
}


//change position of items for iPhone 3.5"
var iPhone5 = (Ti.Platform.displayCaps.platformHeight===568);
if(!iPhone5){
	$.header.top=-60;
	$.headerBack.top=-60;
	$.infoButton.top=15;
	$.lastReadingContainer.top=75;
	$.buttons.top=168;
	$.readingCircles.top=276;
	$.readingCircles.left=15;
	$.dialContainer.left=-15;
	$.saveBtn.top=212;
}

//update the lastest readings for the first time
updateLastReading();

// open stats window
function openStats(e){
	var statsWindow = Alloy.createController('stats').getView();
	Alloy.Globals.navGroup.openWindow(statsWindow);
	e.cancelBubble = true;
}


var infoWin; //variable for current information window

//
function changeColor(value,changeHeader){
	var bgcolor="";
	var infoImage="";
	var headerImage="";
		//Ti.App.fireEvent("app:changeColor",{mycolor:bgcolor});
   bgcolor= Alloy.Globals.getValueColor (value);    

 //ph info button status
 	if(value<6.8){
 		infoImage="images/button-acidic.png";
 		headerImage="images/top-image-acidic.png";
 		infoWin="acidic-info";
 		Ti.App.Properties.setString("phValue","Acidic");
 	}else if(value>=6.8 && value<=7.2){
 		infoImage="images/button-neutral.png";
 		headerImage="images/top-image-neutral.png";
 		infoWin="neutral-info";
 		Ti.App.Properties.setString("phValue","Neutral");
 	}else {
 		infoImage="images/button-alkaline.png";
 		headerImage="images/top-image-alkaline.png";
 		infoWin="alkaline-info";
 		Ti.App.Properties.setString("phValue","Alkaline");
 	}
 	
 	//animate info button only if acidity changed
 	if(Ti.App.Properties.getString("lastStatus","neutral-info")!=infoWin){
 		changeInfoButton(infoImage,headerImage);
 		Ti.App.Properties.setString("lastStatus",infoWin);
 	}
 	
 	//first time is loade update the header accordingly
 	if(changeHeader){
 		changeInfoButton(infoImage,headerImage);
 		Ti.App.Properties.setString("lastStatus",infoWin);
 	}
  $.readingLabel.color= barLabel.color= $.lastReadingTitle.color = $.barsIcon.tint = $.win1.navTintColor = bgcolor;
  $.saveBtn.color=bgcolor;
  $.win1.titleAttributes=  {
        color:bgcolor,
        font: {fontFamily:'Roboto', fontSize:16}
	};
}// ends change color



// updates the bullet colour for the latest entries
function updateBulletColor(value,bullet,arrow){	
	var currentColor=Alloy.Globals.getValueColor (value);
	if(value<6.8){
 		bullet.tint=arrow.tint=edit.tint=currentColor;
 	}else if(value>=6.8 && value<=7.2){
 		bullet.tint=arrow.tint=edit.tint=currentColor;
 	}else {
 		bullet.tint=arrow.tint=edit.tint=currentColor;
 	}
}


//change the buttons the frist time we get in or
function updateStateButton(infoImage,headerImage){
		$.infoButton.image=infoImage;
 		$.infoButton.animate(inner);
 		$.header.image=headerImage;
};

//aminates in and out the button with acidity information
function changeInfoButton(infoImage,headerImage){
	var out = Ti.UI.createAnimation({
    	right : -175,
    	duration : 800
 	 });
 	 
 	 var inner = Ti.UI.createAnimation({
    	right : 0,
    	duration : 800
 	 });
 	 
 	 $.infoButton.animate(out);
 	 
	out.addEventListener("complete",function(evt){
		$.infoButton.image=infoImage;
 		$.infoButton.animate(inner);
 		//$.header.image=headerImage;
 		changeHeader(headerImage);
 		
	});
	
}

// changes the header image depeping if it's acidic, alkaline or neutral
function changeHeader(headerImage){	
	 var fadeImage = Ti.UI.createAnimation({
    	opacity : 0,
    	duration : 800
 	 });
 	 
	$.headerBack.image=headerImage;
	$.header.animate(fadeImage);
	fadeImage.addEventListener("complete",function(evt){
		$.header.image=headerImage;
		$.header.opacity=1;
	});
	
}


// opens the more info screen
function openInfo(evt){
	var infoWindow = Alloy.createController(infoWin).getView();
	Alloy.Globals.navGroup.openWindow(infoWindow);
}
$.infoButton.addEventListener("click", openInfo);


//reset the dial, re enable it 
function resetStatus(){
	$.saveBtn.title="save";
	bar.visible=true;
	$.webViewDial.touchEnabled=true;
	$.sliderGradient.opacity=1;
	
}

Ti.App.addEventListener("resume", resetStatus); // reset dial if the app resumed

//save button is pressed
function saveReading(e){
	if($.saveBtn.title!="recipes"){
		Ti.App.Properties.setString("lastReading",numb);
    	updateLastReading(); //update last reading table with latest reading
    	changeColor(numb); //change button colors
    	animateEditButton(); //make edit button available to edit the lastest entry
   		$.saveBtn.title="recipes";
   		bar.visible=false;
   		$.sliderGradient.opacity=0.4; // make the dial more trasnparent
   		$.webViewDial.touchEnabled=false;
   		//$.saladIcon.visible=true;
   		Ti.App.Properties.setString("currentColour", $.saveBtn.color);
   		var month= moment().format("M");
   		//Ti.API.info("month: " + month);
   		var day=moment().format("DD");
		//Ti.API.info("day of month: "+ day);
		var dow=moment().format("ddd");
		//Ti.API.info("day of week: "+dow);
		var week=moment().format("w");
		//Ti.API.info("week of year: " + week);
		var year = moment().format("YYYY");
		//Ti.API.info("year: " + year);
		Ti.App.Properties.setString("lastused",day+month+year); //lasttime used to change the button back to save if it's a different day.
		
		var db = Ti.Database.open('stats');
		var row = db.execute("SELECT id, day, month, year FROM stats WHERE day="+day+" AND month="+month+" AND year="+year);
		//var row = db.execute("SELECT wpID,modified FROM recipes WHERE wpID="+wpID);
		if(row.isValidRow()){
			//alert("you already saved today");
			//Ti.API.info('id:'+row.fieldByName("id"));
			db.execute("UPDATE stats set value="+ numb +" WHERE id="+row.fieldByName("id"));
		}else{
			
			db.execute("INSERT INTO stats  (value,day,month,year,week, dow) VALUES (?,?,?,?,?,?)",numb, day,month,year,week,dow);
		}
		row.close();
		db.close();

	}else{
		
		 var winTimesOfDay = Alloy.createController('times-of-day').getView();
		//var winTimesOfDay = Alloy.createController('meals').getView();
		Alloy.Globals.navGroup.openWindow(winTimesOfDay);
		//reset the slider
		
   		$.saveBtn.title="recipes";
   		bar.visible=false;
   		$.webViewDial.touchEnabled=false;
   		$.sliderGradient.opacity=0.4;
   		//$.saladIcon.visible=false;
	}
   
};

//help button listener
$.helpView.addEventListener("click", openMoreInfo);

function openMoreInfo(e){
	var moreInfoWin = Alloy.createController('more-info').getView();
	Alloy.Globals.navGroup.openWindow(moreInfoWin);
};

// Open list of recipes that have been favourited
function openFavs(e){
	var favsWin = Alloy.createController('fav-listing').getView();
	Alloy.Globals.navGroup.openWindow(favsWin);
}

// Open all recipes
function openAll(e){
	var args = {
        all : true
    };
	var winTimesOfDay = Alloy.createController('times-of-day',args).getView();
	Alloy.Globals.navGroup.openWindow(winTimesOfDay);
}

// Open window for notification settings
function openNotification(e){
	var winNotifications = Alloy.createController('notifications').getView();
	Alloy.Globals.navGroup.openWindow(winNotifications);
}

// to calculate the angle the dial was moved and adjust the thumb/knob accordinly
var old=0;
Ti.App.addEventListener('app:fromWebView',function(e){
	percent=((e.value-5)/4);
 	//Ti.API.info('percent:'+percent*angleArc);
	t1 = t1.rotate((percent*angleArc)-old);
	old=(percent*angleArc);
	bar.transform=t1;
	numb= e.value;
	numb = numb.toFixed(1);
	$.readingLabel.text= numb;
	barLabel.text=numb;
});

// Listwenr for when the dial is released
Ti.App.addEventListener('app:release',function(e){
	numb= e.value;
	numb = numb.toFixed(1);
	$.readingLabel.text= numb;
	barLabel.text=numb;
	changeColor(numb);
});

//update slider with last recorded value when is finished loaded.
$.webViewDial.addEventListener("load", function(e){
	Ti.API.info("last value: "+Ti.App.Properties.getString("lastReading","7.0"));
	numb = Number(Ti.App.Properties.getString("lastReading","7.0"));
	numb = numb.toFixed(1);
	Ti.App.fireEvent("lastValue",{lastValue:numb});
	$.readingLabel.text= numb;
	barLabel.text=numb;
	changeColor(Ti.App.Properties.getString("lastReading","7.0"),true); // send extra parameter true to update the header image on first load
	
	//move the thumb
	percent=((numb-5)/4);
	t1 = t1.rotate((percent*angleArc)-old);
	old=(percent*angleArc);
	bar.transform=t1;
	
});


// when a new entry is saved we animate the edit button in
function animateEditButton(){
	var o= Ti.UI.createAnimation({
		left:-15,
		time:800,
	});
	
	var i= Ti.UI.createAnimation({
		left:14,
		time:800,
	});
	
	bullet1.animate(o);
	
	o.addEventListener("complete", function(e){
		edit.animate(i);
		editCont.addEventListener("click", function(evt){
			evt.cancelBubble = true;
			sendFeedback(e);
 		});
	});
}


//popUp for editing value
function sendFeedback(e){
	var myModal = Ti.UI.createWindow({
    title           : 'My Modal',
    backgroundColor : 'transparent'
	});
	
	var wrapperView    = Ti.UI.createView(); // Full screen
	var backgroundView = Ti.UI.createView({  // Also full screen
	    backgroundColor : '#000',
	    opacity         : 0.3
	});
	var containerView  = Ti.UI.createView({  // Set height appropriately
	    //height          : 300,
	    //backgroundColor : '#FFF',
	    //opacity: 0.7
	    height:156,
	    width:280,
	    backgroundImage:"images/popUp-small.png"
	});
	
	var preloaderCont = Ti.UI.createView({  // Set height appropriately
	    height          : 300,
	    backgroundColor : '#000',
	    opacity:0.4,
	    zIndex:3,
	    visible:false 
	});
	
	var preloaderLabel = Ti.UI.createLabel({
		text:"sending...",
		font:{fontSize:15,fontFamily:"Roboto-Light"},
		color:"#fff",
		zIndex:4
	});
	
	preloaderCont.add(preloaderLabel);
	
	var title=Ti.UI.createLabel({
		top:13,
		text:"Enter a new value for today's entry (between 5.0 and 9.0)",
		textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
		font:{fontFamily:"Roboto-Light", fontSize:16},
		color:"#ffffff",
		width:220
	});
	
	var textArea = Ti.UI.createTextArea({
	  color: '#fff',
	  backgroundColor:"transparent",
	  font: {fontSize:20, fontFamily:"Roboto-Light"},
	  keyboardType: Titanium.UI.KEYBOARD_NUMBERS_PUNCTUATION,
	  top: 50,
	  width: 230, height : 40
	});
  
	var closeButton    = Ti.UI.createButton({
	    title  : 'Close',
	    bottom : 20,
	    right:50,
	    color:"#ffffff"
	});
	
	closeButton.addEventListener('click', function () {
	    myModal.close();
	});
	
	var sendButton    = Ti.UI.createButton({
	    title  : 'Update',
	    bottom : 20,
	    left:50,
	    color:"#ffffff"
	});
	
	sendButton.addEventListener('click', function(e){
  				  	var month= moment().format("M");
				   	var day=moment().format("DD");
					var year = moment().format("YYYY");
				  	var value=Number(textArea.value);
				  	var db = Ti.Database.open('stats');
  				  	db.execute("UPDATE stats set value="+ value +" WHERE day="+ day  +" AND month="+ month +" AND year="+year);
  				  	updateLastReading(); //update last reading table with latest reading
    				changeColor(value); //change button colors	
    				myModal.close();// close popUp			  	 
	});
			  
	containerView.add(title);
	containerView.add(textArea);
	containerView.add(closeButton);
	containerView.add(sendButton);
	containerView.add(preloaderCont);
	
	wrapperView.add(backgroundView);
	wrapperView.add(containerView);
	
	myModal.add(wrapperView);
	
	
	myModal.open({
    animate : true
	});
	
	myModal.addEventListener("open", function(event, type) {
   		textArea.focus();
	});
} //ends editing value popUp

